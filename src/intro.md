# Introduction

Here you can find Bible translations from [bible-api](https://api.bible.berinaniesh.xyz) in a clean readable format. 

Right now, the following translations are available

- [TOVBSI](bible/TOVBSI/intro.html)
- [KJV](bible/KJV/intro.html)
- [MLSVP](bible/MLSVP/intro.html)
- [ASV](bible/ASV/intro.html)
