# About

This site is made by [Berin Aniesh](https://berinaniesh.xyz) in the hopes that it will be useful to someone.

If you have any comments feel free to [reach out](https://berinaniesh.xyz/contact).


This site is [open source](https://gitlab.com/scripture/scripture.gitlab.io) and everything is released
to the public domain. Feel free to fork and self host.

## Typos / Issues

If you find some typos or have some improvements, feel free to raise a merge request or open issues at the 
GitLab [repo](https://gitlab.com/scripture/scripture.gitlab.io).
