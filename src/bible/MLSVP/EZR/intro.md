## എസ്രാ
- Number of chapters: 10
- Number of verses: 280
## Chapters
- [എസ്രാ 1](/bible/MLSVP/EZR/1.html)
- [എസ്രാ 2](/bible/MLSVP/EZR/2.html)
- [എസ്രാ 3](/bible/MLSVP/EZR/3.html)
- [എസ്രാ 4](/bible/MLSVP/EZR/4.html)
- [എസ്രാ 5](/bible/MLSVP/EZR/5.html)
- [എസ്രാ 6](/bible/MLSVP/EZR/6.html)
- [എസ്രാ 7](/bible/MLSVP/EZR/7.html)
- [എസ്രാ 8](/bible/MLSVP/EZR/8.html)
- [എസ്രാ 9](/bible/MLSVP/EZR/9.html)
- [എസ്രാ 10](/bible/MLSVP/EZR/10.html)
