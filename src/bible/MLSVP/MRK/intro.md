## മർക്കൊസ്
- Number of chapters: 16
- Number of verses: 678
## Chapters
- [മർക്കൊസ് 1](/bible/MLSVP/MRK/1.html)
- [മർക്കൊസ് 2](/bible/MLSVP/MRK/2.html)
- [മർക്കൊസ് 3](/bible/MLSVP/MRK/3.html)
- [മർക്കൊസ് 4](/bible/MLSVP/MRK/4.html)
- [മർക്കൊസ് 5](/bible/MLSVP/MRK/5.html)
- [മർക്കൊസ് 6](/bible/MLSVP/MRK/6.html)
- [മർക്കൊസ് 7](/bible/MLSVP/MRK/7.html)
- [മർക്കൊസ് 8](/bible/MLSVP/MRK/8.html)
- [മർക്കൊസ് 9](/bible/MLSVP/MRK/9.html)
- [മർക്കൊസ് 10](/bible/MLSVP/MRK/10.html)
- [മർക്കൊസ് 11](/bible/MLSVP/MRK/11.html)
- [മർക്കൊസ് 12](/bible/MLSVP/MRK/12.html)
- [മർക്കൊസ് 13](/bible/MLSVP/MRK/13.html)
- [മർക്കൊസ് 14](/bible/MLSVP/MRK/14.html)
- [മർക്കൊസ് 15](/bible/MLSVP/MRK/15.html)
- [മർക്കൊസ് 16](/bible/MLSVP/MRK/16.html)
