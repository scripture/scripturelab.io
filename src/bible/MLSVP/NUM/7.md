# സംഖ്യാപുസ്തകം 7
```
1 മോശെ തിരുനിവാസം നിവിർത്തുകഴിഞ്ഞിട്ടു അതും അതിന്റെ ഉപകരണങ്ങളൊക്കെയും അഭിഷേകം ചെയ്തു ശുദ്ധീകരിക്കയും യാഗപീഠത്തെയും അതിന്റെ സകലപാത്രങ്ങളെയും അഭിഷേകം കഴിച്ചു ശുദ്ധീകരിക്കയും ചെയ്ത ദിവസം
```
```
2 തങ്ങളുടെ പിതൃഭവനങ്ങളിൽ പ്രധാനികളും ഗോത്രപ്രഭുക്കന്മാരും എണ്ണപ്പെട്ടവരുടെ മേൽവിചാരകന്മാരും ആയ യിസ്രായേൽപ്രഭുക്കന്മാർ വഴിപാടു കഴിച്ചു.
```
```
3 അവർ വഴിപാടായിട്ടു ഈരണ്ടു പ്രഭുക്കന്മാർ ഓരോ വണ്ടിയും ഓരോരുത്തൻ ഓരോ കാളയും ഇങ്ങനെ കൂടുള്ള ആറു വണ്ടിയും പന്ത്രണ്ടു കാളയും യഹോവയുടെ സന്നിധിയിൽ തിരുനിവാസത്തിന്റെ മുമ്പിൽ കൊണ്ടുവന്നു.
```
```
4 അപ്പോൾ യഹോവ മോശെയോടു:
```
```
5 അവരുടെ പക്കൽനിന്നു അവയെ വാങ്ങുക. അവ സമാഗമനകൂടാരത്തിന്റെ ഉപയോഗത്തിന്നു ഇരിക്കട്ടെ; അവയെ ലേവ്യരിൽ ഓരോരുത്തന്നു അവനവന്റെ വേലക്കു തക്കവണ്ണം കൊടുക്കേണം എന്നു കല്പിച്ചു.
```
```
6 മോശെ വണ്ടികളെയും കാളകളെയും വാങ്ങി ലേവ്യർക്കു കൊടുത്തു.
```
```
7 രണ്ടു വണ്ടിയും നാലു കാളയെയും അവൻ  ഗേർശോന്യർക്കു അവരുടെ വേലെക്കു തക്കവണ്ണം കൊടുത്തു.
```
```
8 നാലു വണ്ടിയും എട്ടു കാളയെയും അവൻ മെരാര്യർക്കു പുരോഹിതനായ അഹരോന്റെ പുത്രൻ ഈഥാമാരിന്റെ കൈക്കീഴ് അവർക്കുള്ള വേലെക്കു തക്കവണ്ണം കൊടുത്തു.
```
```
9 കെഹാത്യർക്കു അവൻ  ഒന്നും കൊടുത്തില്ല; അവരുടെ വേല വിശുദ്ധമന്ദിരം സംബന്ധിച്ചുള്ളതും തോളിൽ ചുമക്കുന്നതും ആയിരുന്നു.
```
```
10 യാഗപീഠം അഭിഷേകം ചെയ്ത ദിവസം പ്രഭുക്കന്മാർ പ്രതിഷ്ഠെക്കുള്ള വഴിപാടു കൊണ്ടുവന്നു; യാഗപീഠത്തിന്റെ മുമ്പാകെ പ്രഭുക്കന്മാർ തങ്ങളുടെ വഴിപാടു കൊണ്ടുവന്നു.
```
```
11 അപ്പോൾ യഹോവ മോശെയോടു: യാഗപീഠത്തിന്റെ പ്രതിഷ്ഠെക്കായി ഓരോ പ്രഭു ഓരോ ദിവസം താന്താന്റെ വഴിപാടു കൊണ്ടുവരേണം എന്നു കല്പിച്ചു.
```
```
12 ഒന്നാം ദിവസം വഴിപാടു കഴിച്ചവൻ യെഹൂദാഗോത്രത്തിൽ അമ്മീനാദാബിന്റെ മകനായ നഹശോൻ.
```
```
13 അവന്റെ വഴിപാടു വിശുദ്ധമന്ദിരത്തിലെ തൂക്കപ്രകാരം നൂറ്റിമുപ്പതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിത്തളിക, എഴുപതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിക്കിണ്ണം - അവ രണ്ടും ഭോജനയാഗത്തിന്നായി എണ്ണചേർത്ത നേരിയ മാവുകൊണ്ടു നിറഞ്ഞിരുന്നു -
```
```
14 ധൂപവർഗ്ഗം നിറഞ്ഞതും പത്തു ശേക്കെൽ തൂക്കം ഉള്ളതുമായ ഒരു പൊൻകലശം,
```
```
15 ഹോമയാഗത്തിന്നായി ഒരു കാളക്കിടാവു, ഒരു ആട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള ഒരു ആൺകുഞ്ഞാടു,
```
```
16 പാപയാഗത്തിന്നായി ഒരു കോലാട്ടുകൊറ്റൻ, സമാധാനയാഗത്തിന്നായി രണ്ടു കാള,
```
```
17 അഞ്ചു ആട്ടുകൊറ്റൻ, അഞ്ചു കോലാട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള അഞ്ചു ചെമ്മരിയാട്ടിൻകുട്ടി; ഇതു അമ്മീനാദാബിന്റെ മകനായ നഹശോന്റെ വഴിപാടു.
```
```
18 രണ്ടാം ദിവസം യിസ്സാഖാരിന്റെ മക്കളുടെ പ്രഭുവായ സൂവാരിന്റെ മകൻ നെഥനയേൽ വഴിപാടു കഴിച്ചു.
```
```
19 അവൻ  വഴിപാടു കഴിച്ചതു: വിശുദ്ധമന്ദിരത്തിലെ തൂക്കപ്രകാരം നൂറ്റിമുപ്പതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിത്തളിക, എഴുപതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിക്കിണ്ണം - ഇവ രണ്ടും ഭോജനയാഗത്തിന്നായി എണ്ണചേർത്ത നേരിയ മാവുകൊണ്ടു നിറഞ്ഞിരുന്നു -
```
```
20 ധൂപവർഗ്ഗം നിറഞ്ഞതും പത്തു ശേക്കെൽ തൂക്കമുള്ളതുമായ ഒരു പൊൻകലശം,
```
```
21 ഹോമയാഗത്തിന്നായി ഒരു കാളക്കിടാവു, ഒരു ആട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള ഒരു കുഞ്ഞാടു,
```
```
22 പാപയാഗത്തിന്നായി ഒരു കോലാട്ടുകൊറ്റൻ,
```
```
23 സമാധാനയാഗത്തിന്നായി രണ്ടു കാള, അഞ്ചു ആട്ടുകൊറ്റൻ, അഞ്ചു കോലാട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള അഞ്ചു കുഞ്ഞാടു; ഇതു സൂവാരിന്റെ മകനായ നെഥനയേലിന്റെ വഴിപാടു.
```
```
24 മൂന്നാം ദിവസം സെബൂലൂന്റെ മക്കളുടെ പ്രഭുവായ ഹേലോന്റെ മകൻ എലീയാബ് വഴിപാടു കഴിച്ചു.
```
```
25 അവന്റെ വഴിപാടു വിശുദ്ധമന്ദിരത്തിലെ തൂക്കപ്രകാരം നൂറ്റിമുപ്പതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിത്തളിക, എഴുപതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിക്കിണ്ണം - ഇവ രണ്ടും ഭോജനയാഗത്തിന്നായി എണ്ണചേർത്ത നേരിയമാവുകൊണ്ടു നിറഞ്ഞിരുന്നു -
```
```
26 ധൂപവർഗ്ഗം നിറഞ്ഞതും പത്തു ശേക്കെൽ തൂക്കം ഉള്ളതുമായ ഒരു പൊൻകലശം,
```
```
27 ഹോമയാഗത്തിന്നായി ഒരു കാളക്കിടാവു, ഒരു ആട്ടുകൊറ്റൻ; ഒരു വയസ്സു പ്രായമുള്ള ഒരു കുഞ്ഞാടു,
```
```
28 പാപയാഗത്തിന്നായി ഒരു കോലാട്ടുകൊറ്റൻ,
```
```
29 സമാധാനയാഗത്തിന്നായി രണ്ടു കാള, അഞ്ചു ആട്ടുകൊറ്റൻ, അഞ്ചു കോലാട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള അഞ്ചു കുഞ്ഞാടു; ഇതു ഹേലോന്റെ മകൻ എലീയാബിന്റെ വഴിപാടു.
```
```
30 നാലാം ദിവസം രൂബേന്റെ മക്കളുടെ പ്രഭുവായ ശെദേയൂരിന്റെ മകൻ എലീസൂർ വഴിപാടു കഴിച്ചു.
```
```
31 അവന്റെ വഴിപാടു വിശുദ്ധമന്ദിരത്തിലെ തൂക്കപ്രകാരം നൂറ്റി മുപ്പതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിത്തളിക, എഴുപതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിക്കിണ്ണം - ഇവ രണ്ടും ഭോജനയാഗത്തിന്നായി എണ്ണചേർത്ത നേരിയ മാവുകൊണ്ടു നിറഞ്ഞിരുന്നു -
```
```
32 ധൂപവർഗ്ഗം നിറഞ്ഞതും പത്തു ശേക്കെൽ തൂക്കമുള്ളതുമായ ഒരു പൊൻകലശം,
```
```
33 ഹോമയാഗത്തിന്നായി ഒരു കാളക്കിടാവു, ഒരു ആട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള ഒരു കുഞ്ഞാടു,
```
```
34 പാപയാഗത്തിന്നായി ഒരു കോലാട്ടുകൊറ്റൻ,
```
```
35 സമാധാനയാഗത്തിന്നായി രണ്ടു കാള, അഞ്ചു ആട്ടുകൊറ്റൻ, അഞ്ചു കോലാട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള അഞ്ചു കുഞ്ഞാടു; ഇതു ശെദേയൂരിന്റെ മകൻ എലീസൂരിന്റെ വഴിപാടു.
```
```
36 അഞ്ചാം ദിവസം ശിമെയോന്റെ മക്കളുടെ പ്രഭുവായ സൂരീശദ്ദായിയുടെ മകൻ ശെലൂമീയേൽ വഴിപാടു കഴിച്ചു.
```
```
37 അവന്റെ വഴിപാടു വിശുദ്ധമന്ദിരത്തിലെ തൂക്കപ്രകാരം നൂറ്റിമുപ്പതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിത്തളിക, എഴുപതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിക്കിണ്ണം - ഇവ രണ്ടും ഭോജനയാഗത്തിന്നായി എണ്ണചേർത്ത നേരിയ മാവുകൊണ്ടു നിറഞ്ഞിരുന്നു -
```
```
38 ധൂപവർഗ്ഗം നിറഞ്ഞതും പത്തു ശേക്കെൽ തൂക്കമുള്ളതുമായ ഒരു പൊൻകലശം,
```
```
39 ഹോമയാഗത്തിന്നായി ഒരു കാളക്കിടാവു, ഒരു ആട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള ഒരു കുഞ്ഞാടു,
```
```
40 പാപയാഗത്തിന്നായി ഒരു കോലാട്ടുകൊറ്റൻ, സമാധാനയാഗത്തിന്നായി രണ്ടു കാള,
```
```
41 അഞ്ചു ആട്ടുകൊറ്റൻ, അഞ്ചു കോലാട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള അഞ്ചു കുഞ്ഞാടു. ഇതു സൂരീശദ്ദായിയുടെ മകൻ ശെലൂമീയേലിന്റെ വഴിപാടു.
```
```
42 ആറാം ദിവസം ഗാദിന്റെ മക്കളുടെ പ്രഭുവായ ദെയൂവേലിന്റെ മകൻ എലീയാസാഫ് വഴിപാടു കഴിച്ചു.
```
```
43 അവന്റെ വഴിപാടു വിശുദ്ധമന്ദിരത്തിലെ തൂക്കപ്രകാരം നൂറ്റിമുപ്പതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിത്തളിക, എഴുപതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിക്കിണ്ണം - ഇവ രണ്ടും ഭോജനയാഗത്തിന്നായി എണ്ണചേർത്ത നേരിയ മാവുകൊണ്ടു നിറഞ്ഞിരുന്നു -
```
```
44 ധൂപവർഗ്ഗം നിറഞ്ഞതും പത്തു ശേക്കെൽ തൂക്കമുള്ളതുമായ ഒരു പൊൻകലശം,
```
```
45 ഹോമയാഗത്തിന്നായി ഒരു കാളക്കിടാവു, ഒരു ആട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള ഒരു കുഞ്ഞാടു,
```
```
46 പാപയാഗത്തിന്നായി ഒരു കോലാട്ടുകൊറ്റൻ,
```
```
47 സമാധാനയാഗത്തിന്നായി രണ്ടു കാള, അഞ്ചു ആട്ടുകൊറ്റൻ, അഞ്ചു കോലാട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള അഞ്ചു കുഞ്ഞാടു. ഇതു ദെയൂവേലിന്റെ മകൻ എലീയാസാഫിന്റെ വഴിപാടു.
```
```
48 ഏഴാം ദിവസം എഫ്രയീമിന്റെ മക്കളുടെ പ്രഭുവായ അമ്മീഹൂദിന്റെ മകൻ എലീശാമാ വഴിപാടു കഴിച്ചു.
```
```
49 അവന്റെ വഴിപാടു വിശുദ്ധമന്ദിരത്തിലെ തൂക്കപ്രകാരം നൂറ്റിമുപ്പതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിത്തളിക, എഴുപതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിക്കിണ്ണം - ഇവ രണ്ടും ഭോജനയാഗത്തിന്നായി എണ്ണചേർത്ത നേരിയമാവുകൊണ്ടു നിറഞ്ഞിരുന്നു -
```
```
50 ധൂപവർഗ്ഗം നിറഞ്ഞതും പത്തു ശേക്കെൽ തൂക്കം ഉള്ളതുമായ ഒരു പൊൻകലശം,
```
```
51 ഹോമയാഗത്തിന്നായി ഒരു കാളക്കിടാവു, ഒരു ആട്ടുകൊറ്റൻ, ഒരു വയസ്സുപ്രായമുള്ള ഒരു കുഞ്ഞാടു,
```
```
52 പാപയാഗത്തിന്നായി ഒരു കോലാട്ടുകൊറ്റൻ,
```
```
53 സമാധാനയാഗത്തിന്നായി രണ്ടു കാള, അഞ്ചു ആട്ടുകൊറ്റൻ, അഞ്ചു കോലാട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള അഞ്ചു കുഞ്ഞാടു. ഇതു അമ്മീഹൂദിന്റെ മകൻ എലീശാമായുടെ വഴിപാടു.
```
```
54 എട്ടാം ദിവസം മനശ്ശെയുടെ മക്കളുടെ പ്രഭുവായ പെദാസൂരിന്റെ മകൻ ഗമലീയേൽ വഴിപാടു കഴിച്ചു.
```
```
55 അവന്റെ വഴിപാടു വിശുദ്ധമന്ദിരത്തിലെ തൂക്കപ്രകാരം നൂറ്റിമുപ്പതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിത്തളിക, എഴുപതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിക്കിണ്ണം - ഇവരണ്ടും ഭോജനയാഗത്തിന്നായി എണ്ണ ചേർത്ത നേരിയ മാവുകൊണ്ടു നിറഞ്ഞിരുന്നു -
```
```
56 ധൂപവർഗ്ഗം നിറഞ്ഞതും പത്തു ശേക്കെൽ തൂക്കമുള്ളതുമായ ഒരു പൊൻകലശം,
```
```
57 ഹോമയാഗത്തിന്നായി ഒരു കാളക്കിടാവു, ഒരു ആട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള ഒരു കുഞ്ഞാടു,
```
```
58 പാപയാഗത്തിന്നായി ഒരു കോലാട്ടുകൊറ്റൻ,
```
```
59 സമാധാനയാഗത്തിന്നായി രണ്ടു കാള, അഞ്ചു ആട്ടുകൊറ്റൻ, അഞ്ചു കോലാട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള അഞ്ചു കുഞ്ഞാടു; ഇതു പെദാസൂരിന്റെ മകൻ ഗമലീയേലിന്റെ വഴിപാടു.
```
```
60 ഒമ്പതാം ദിവസം ബെന്യാമീന്റെ മക്കളുടെ പ്രഭുവായ ഗിദെയോനിയുടെ മകൻ അബീദാൻ വഴിപാടു കഴിച്ചു.
```
```
61 അവന്റെ വഴിപാടു വിശുദ്ധമന്ദിരത്തിലെ തൂക്കപ്രകാരം നൂറ്റിമുപ്പതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിത്തളിക, എഴുപതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിക്കിണ്ണം - ഇവ രണ്ടും ഭോജനയാഗത്തിന്നായി എണ്ണ ചേർത്ത നേരിയ മാവുകൊണ്ടു നിറഞ്ഞിരുന്നു -
```
```
62 ധൂപവർഗ്ഗം നിറഞ്ഞതും പത്തു ശേക്കെൽ തൂക്കമുള്ളതുമായ ഒരു പൊൻകലശം,
```
```
63 ഹോമയാഗത്തിന്നായി, ഒരു കാളക്കിടാവു, ഒരു ആട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള ഒരു കുഞ്ഞാടു,
```
```
64 പാപയാഗത്തിന്നായി ഒരു കോലാട്ടുകൊറ്റൻ,
```
```
65 സമാധാനയാഗത്തിന്നായി രണ്ടു കാള, അഞ്ചു ആട്ടുകൊറ്റൻ, അഞ്ചു കോലാട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള അഞ്ചു കുഞ്ഞാടു. ഇതു ഗിദെയോനിയുടെ മകൻ അബീദാന്റെ വഴിപാടു.
```
```
66 പത്താം ദിവസം ദാന്റെ മക്കളുടെ പ്രഭുവായ അമ്മീശദ്ദായിയുടെ മകൻ അഹീയേസെർ വഴിപാടു കഴിച്ചു.
```
```
67 അവന്റെ വഴിപാടു വിശുദ്ധമന്ദിരത്തിലെ തൂക്കപ്രകാരം നൂറ്റിമുപ്പതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിത്തളിക, എഴുപതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിക്കിണ്ണം - ഇവ രണ്ടും ഭോജനയാഗത്തിന്നായി എണ്ണ ചേർത്ത നേരിയ മാവുകൊണ്ടു നിറഞ്ഞിരുന്നു -
```
```
68 ധൂപവർഗ്ഗം നിറഞ്ഞതും പത്തു ശേക്കെൽ തൂക്കം ഉള്ളതുമായ ഒരു പൊൻകലശം,
```
```
69 ഹോമയാഗത്തിന്നായി ഒരു കാളക്കിടാവു, ഒരു ആട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള ഒരു കുഞ്ഞാടു,
```
```
70 പാപയാഗത്തിന്നായി ഒരു കോലാട്ടുകൊറ്റൻ,
```
```
71 സമാധാനയാഗത്തിന്നായി രണ്ടു കാള, അഞ്ചു ആട്ടുകൊറ്റൻ, അഞ്ചു കോലാട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള അഞ്ചു കുഞ്ഞാടു. ഇതു അമ്മീശദ്ദായിയുടെ മകൻ അഹീയേസെരിന്റെ വഴിപാടു.
```
```
72 പതിനൊന്നാം ദിവസം ആശേരിന്റെ മക്കളുടെ പ്രഭുവായ ഒക്രാന്റെ മകൻ പഗീയേൽ വഴിപാടു കഴിച്ചു.
```
```
73 അവന്റെ വഴിപാടു വിശുദ്ധമന്ദിരത്തിലെ തൂക്കപ്രകാരം നൂറ്റിമുപ്പതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിത്തളിക, എഴുപതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിക്കിണ്ണം - ഇവ രണ്ടും ഭോജനയാഗത്തിന്നായി എണ്ണചേർത്ത നേരിയ മാവുകൊണ്ടു നിറഞ്ഞിരുന്നു -
```
```
74 ധൂപവർഗ്ഗം നിറഞ്ഞതും പത്തു ശേക്കെൽ തൂക്കമുള്ളതുമായ ഒരു പൊൻകലശം,
```
```
75 ഹോമയാഗത്തിന്നായി ഒരു കാളക്കിടാവു, ഒരു ആട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള ഒരു കുഞ്ഞാടു,
```
```
76 പാപയാഗത്തിന്നായി ഒരു കോലാട്ടുകൊറ്റൻ,
```
```
77 സമാധാനയാഗത്തിന്നായി രണ്ടു കാള, അഞ്ചു ആട്ടുകൊറ്റൻ, അഞ്ചു കോലാട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള അഞ്ചു കുഞ്ഞാടു. ഇതു ഒക്രാന്റെ മകനായ പഗീയേലിന്റെ വഴിപാടു.
```
```
78 പന്ത്രണ്ടാം ദിവസം നഫ്താലിയുടെ മക്കളുടെ പ്രഭുവായ ഏനാന്റെ മകൻ അഹീര വഴിപാടു കഴിച്ചു.
```
```
79 അവന്റെ വഴിപാടു വിശുദ്ധമന്ദിരത്തിലെ തൂക്കപ്രകാരം നൂറ്റിമുപ്പതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിത്തളിക, എഴുപതു ശേക്കെൽ തൂക്കമുള്ള ഒരു വെള്ളിക്കിണ്ണം - ഇവരണ്ടും ഭോജനയാഗത്തിന്നായി എണ്ണചേർത്ത നേരിയ മാവുകൊണ്ടു നിറഞ്ഞിരുന്നു -
```
```
80 ധൂപവർഗ്ഗം നിറഞ്ഞതും പത്തു ശേക്കെൽ തൂക്കമുള്ളതുമായ ഒരു പൊൻകലശം,
```
```
81 ഹോമയാഗത്തിന്നായി ഒരു കാളക്കിടാവു, ഒരു ആട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള ഒരു കുഞ്ഞാടു,
```
```
82 പാപയാഗത്തിന്നായി ഒരു കോലാട്ടുകൊറ്റൻ,
```
```
83 സമാധാനയാഗത്തിന്നായി രണ്ടു കാള, അഞ്ചു ആട്ടുകൊറ്റൻ, അഞ്ചു കോലാട്ടുകൊറ്റൻ, ഒരു വയസ്സു പ്രായമുള്ള അഞ്ചു കുഞ്ഞാടു. ഇതു ഏനാന്റെ മകൻ അഹീരയുടെ വഴിപാടു.
```
```
84 യാഗപീഠം അഭിഷേകം ചെയ്ത ദിവസം യിസ്രായേൽ പ്രഭുക്കന്മാരുടെ പ്രതിഷ്ഠവഴിപാടു ഇതു ആയിരുന്നു; വെള്ളിത്തളിക പന്ത്രണ്ടു, വെള്ളിക്കിണ്ണം പന്ത്രണ്ടു,
```
```
85 പൊൻകലശം പന്ത്രണ്ടു, വെള്ളിത്തളിക ഒന്നിന്നു തൂക്കം നൂറ്റിമുപ്പതു ശേക്കെൽ; കിണ്ണം ഒന്നിന്നു എഴുപതു ശേക്കെൽ; ഇങ്ങനെ വെള്ളിപ്പാത്രങ്ങൾ ആകെ വിശുദ്ധമന്ദിരത്തിലെ തൂക്കപ്രകാരം രണ്ടായിരത്തി നാനൂറു ശേക്കെൽ.
```
```
86 ധൂപവർഗ്ഗം നിറഞ്ഞ പൊൻകലശം പന്ത്രണ്ടു; ഓരോന്നു വിശുദ്ധമന്ദിരത്തിലെ തൂക്കപ്രകാരം പത്തു ശേക്കെൽ വീതം കലശങ്ങളുടെ പൊന്നു ആകെ നൂറ്റിരുപതു ശേക്കെൽ.
```
```
87 ഹോമയാഗത്തിന്നുള്ള നാൽക്കാലികൾ എല്ലാംകൂടി കാളക്കിടാവു പന്ത്രണ്ടു, ആട്ടുകൊറ്റൻ പന്ത്രണ്ടു, ഒരു വയസ്സു പ്രായമുള്ള കുഞ്ഞാടു പന്ത്രണ്ടു, അവയുടെ ഭോജനയാഗം, പാപയാഗത്തിന്നായി കോലാട്ടുകൊറ്റൻ പന്ത്രണ്ടു;
```
```
88 സമാധാനയാഗത്തിന്നായി നാൽക്കാലികൾ എല്ലാംകൂടി കാള ഇരുപത്തിനാലു, ആട്ടുകൊറ്റൻ അറുപതു, കോലാട്ടുകൊറ്റൻ അറുപതു, ഒരു വയസ്സു പ്രായമുള്ള കുഞ്ഞാടു അറുപതു; യാഗപീഠത്തെ അഭിഷേകം ചെയ്തശേഷം അതിന്റെ പ്രതിഷ്ഠെക്കുള്ള വഴിപാടു ഇതു തന്നേ.
```
```
89 മോശെ തിരുമുമ്പിൽ സംസാരിപ്പാൻ സമാഗമനകൂടാരത്തിൽ കടക്കുമ്പോൾ അവൻ  സാക്ഷ്യപെട്ടകത്തിന്മേലുള്ള കൃപാസനത്തിങ്കൽ നിന്നു രണ്ടു കെരൂബുകളുടെ നടുവിൽനിന്നു തന്നോടു സംസാരിക്കുന്ന തിരുശബ്ദം കേട്ടു; അങ്ങനെ അവൻ  അവനോടു സംസാരിച്ചു.
```
