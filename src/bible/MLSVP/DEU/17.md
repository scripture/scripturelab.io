# ആവർത്തനപുസ്തകം 17
```
1 വല്ല ഊനമോ വിരൂപതയോ ഉള്ള കാളയെ എങ്കിലും ആടിനെ എങ്കിലും നിന്റെ ദൈവമായ യഹോവെക്കു യാഗം കഴിക്കരുതു; അതു നിന്റെ ദൈവമായ യഹോവെക്കു വെറുപ്പു ആകുന്നു.
```
```
2 നിന്റെ ദൈവമായ യഹോവ നിനക്കു തരുന്ന ഏതൊരു പട്ടണത്തിലെങ്കിലും നിന്റെ ദൈവമായ യഹോവെക്കു അനിഷ്ടമായുള്ളതു ചെയ്തു അവന്റെ നിയമം ലംഘിക്കയും
```
```
3 ഞാൻ കല്പിച്ചിട്ടില്ലാത്ത അന്യദൈവങ്ങളെയോ സൂര്യചന്ദ്രന്മാരെയോ ആകാശത്തിലെ ശേഷം സൈന്യത്തെയോ ചെന്നു സേവിച്ചു നമസ്കരിക്കയും ചെയ്ത പുരുഷനെയാകട്ടെ സ്ത്രീയെയാകട്ടെ നിങ്ങളുടെ ഇടയിൽ കണ്ടുപിടിക്കയും
```
```
4 അതിനെക്കുറിച്ചു നിനക്കു അറിവുകിട്ടുകയും ചെയ്താൽ നീ നല്ലവണ്ണം ശോധനകഴിച്ചു അങ്ങനെയുള്ള മ്ലേച്ഛത യിസ്രായേലിൽ നടന്നു എന്നുള്ളതു വാസ്തവവും കാര്യം യഥാർത്ഥവും എന്നു കണ്ടാൽ
```
```
5 ആ ദുഷ്ടകാര്യം ചെയ്ത പുരുഷനെയോ സ്ത്രീയെയോ പട്ടണവാതിലിന്നു പുറത്തു കൊണ്ടുപോയി കല്ലെറിഞ്ഞു കൊല്ലേണം.
```
```
6 മരണയോഗ്യനായവനെ കൊല്ലുന്നതു രണ്ടോ മൂന്നോ സാക്ഷികളുടെ വാമൊഴിമേൽ ആയിരിക്കേണം; ഏകസാക്ഷിയുടെ വാമൊഴിമേൽ അവനെ കൊല്ലരുതു.
```
```
7 അവനെ കൊല്ലേണ്ടതിന്നു ആദ്യം സാക്ഷികളുടെയും പിന്നെ സർവ്വജനത്തിന്റെയും കൈ അവന്റെമേൽ ചെല്ലേണം; ഇങ്ങനെ നിങ്ങളുടെ ഇടയിൽനിന്നു ദോഷം നീക്കിക്കളയേണം.
```
```
8 നിന്റെ പട്ടണങ്ങളിൽ കുലപാതകമാകട്ടെ വസ്തുസംബന്ധമായ വ്യവഹാരമാകട്ടെ അടികലശലാകട്ടെ ഇങ്ങിനെയുള്ള ആവലാധികാര്യങ്ങളിൽ വല്ലതും വിധിപ്പാൻ നിനക്കു പ്രയാസം ഉണ്ടായാൽ നീ പുറപ്പെട്ടു നിന്റെ ദൈവമായ യഹോവ തിരഞ്ഞെടുക്കുന്ന സ്ഥലത്തു പോകേണം.
```
```
9 ലേവ്യരായ പുരോഹിതന്മാരുടെ അടുക്കലും അന്നുള്ള ന്യായാധിപന്റെ അടുക്കലും ചെന്നു ചോദിക്കേണം; അവർ നിനക്കു വിധി പറഞ്ഞുതരും.
```
```
10 യഹോവ തിരഞ്ഞെടുക്കുന്ന സ്ഥലത്തുനിന്നു അവർ പറഞ്ഞുതരുന്ന വിധിപോലെ നീ ചെയ്യേണം; അവർ ഉപദേശിച്ചുതരുന്നതുപോലെ ഒക്കെയും ചെയ്‌വാൻ ജാഗ്രതയായിരിക്കേണം.
```
```
11 അവർ ഉപദേശിച്ചുതരുന്ന പ്രമാണത്തിന്നും പറഞ്ഞുതരുന്ന വിധിക്കും അനുസരണയായി നീ ചെയ്യേണം; അവർ പറഞ്ഞുതരുന്ന വിധി വിട്ടു നീ ഇടത്തോട്ടൊ വലത്തോട്ടൊ മാറരുതു.
```
```
12 നിന്റെ ദൈവമായ യഹോവെക്കു അവിടെ ശുശ്രൂഷ ചെയുതുനില്ക്കുന്ന പുരോഹിതന്റെയോ ന്യായാധിപന്റെയോ വാക്കു കേൾക്കാതെ ആരെങ്കിലും അഹങ്കാരം കാണിച്ചാൽ അവൻ മരിക്കേണം; ഇങ്ങനെ യിസ്രായേലിൽനിന്നു ദോഷം നീക്കിക്കളയേണം.
```
```
13 ഇനി അഹങ്കാരം കാണിക്കാതിരിക്കേണ്ടതിന്നു ജനമെല്ലാം കേട്ടു ഭയപ്പെടേണം.
```
```
14 നിന്റെ ദൈവമായ യഹോവ നിനക്കു തരുന്ന ദേശത്തു നീ ചെന്നു അതിനെ കൈവശമാക്കി അവിടെ കുടിപാർത്ത ശേഷം: എന്റെ ചുറ്റമുള്ള സകലജാതികളെയുംപോലെ ഞാൻ ഒരു രാജാവിനെ എന്റെമേൽ ആക്കുമെന്നു പറയുമ്പോൾ
```
```
15 നിന്റെ ദൈവമായ യഹോവ തിരഞ്ഞെടുക്കുന്ന രാജാവിനെ നിന്റെമേൽ ആക്കേണം; നിന്റെ സഹോദരന്മാരുടെ ഇടയിൽനിന്നു ഒരുത്തനെ നിന്റെമേൽ രാജാവാക്കേണം; നിന്റെ സഹോദരനല്ലാത്ത അന്യജാതിക്കാരനെ നിന്റെമേൽ ആക്കിക്കൂടാ.
```
```
16 എന്നാൽ അവന്നു കുതിര അനവധി ഉണ്ടാകരുതു. അധികം കുതിര സമ്പാദിക്കേണ്ടതിന്നു ജനം മിസ്രയീമിലേക്കു മടങ്ങിപ്പോകുവാൻ അവൻ ഇടവരുത്തരുതു; ഇനിമേൽ ആ വഴിക്കു തിരിയരുതു എന്നു യഹോവ നിങ്ങളോടു കൽപ്പിച്ചിട്ടുണ്ടല്ലോ.
```
```
17 അവന്റെ ഹൃദയം മറിഞ്ഞുപോകാതിരിപ്പാൻ അനേകം ഭാര്യമാരെ അവൻ എടുക്കരുതു; വെള്ളിയും പൊന്നും അധികമായി സമ്പാദിക്കയും അരുതു.
```
```
18 അവൻ തന്റെ രാജാസനത്തിൽ ഇരിക്കുമ്പോൾ ലേവ്യരായ പുരോഹിതന്മാരുടെ പക്കൽനിന്നു ഈ ന്യായപ്രമാണം വാങ്ങി അതിന്റെ ഒരു പകർപ്പു ഒരു പുസ്തകത്തിൽ എഴുതി എടുക്കേണം.
```
```
19 ഈ ന്യായപ്രമാണത്തിലെ സകലവചനങ്ങളും ചട്ടങ്ങളും അവൻ പ്രമാണിച്ചുനടന്നു തന്റെ ദൈവമായ യഹോവയെ ഭയപ്പെടുവാൻ പഠിക്കേണ്ടതിന്നു അതു അവന്റെ കൈവശം ഇരിക്കുകയും
```
```
20 അവന്റെ ഹൃദയം സഹോദരന്മാർക്കു മീതെ അഹങ്കരിച്ചുയരാതെയും അവൻ കല്പന വിട്ടു ഇടത്തോട്ടൊ വലത്തോട്ടൊ തിരിയാതെയും ഇരിക്കേണ്ടതിന്നും അവനും അവന്റെ പുത്രന്മാരും യിസ്രായേലിന്റെ ഇടയിൽ ദീർഘകാലം രാജ്യഭാരം ചെയ്യേണ്ടതിന്നുമായി അവൻ തന്റെ ആയുഷ്ക്കാലം ഒക്കെയും അതു വായിക്കയും വേണം.
```
