## 1 തെസ്സലൊനീക്യർ
- Number of chapters: 5
- Number of verses: 89
## Chapters
- [1 തെസ്സലൊനീക്യർ 1](/bible/MLSVP/1TH/1.html)
- [1 തെസ്സലൊനീക്യർ 2](/bible/MLSVP/1TH/2.html)
- [1 തെസ്സലൊനീക്യർ 3](/bible/MLSVP/1TH/3.html)
- [1 തെസ്സലൊനീക്യർ 4](/bible/MLSVP/1TH/4.html)
- [1 തെസ്സലൊനീക്യർ 5](/bible/MLSVP/1TH/5.html)
