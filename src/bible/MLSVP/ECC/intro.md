## സഭാപ്രസംഗി
- Number of chapters: 12
- Number of verses: 222
## Chapters
- [സഭാപ്രസംഗി 1](/bible/MLSVP/ECC/1.html)
- [സഭാപ്രസംഗി 2](/bible/MLSVP/ECC/2.html)
- [സഭാപ്രസംഗി 3](/bible/MLSVP/ECC/3.html)
- [സഭാപ്രസംഗി 4](/bible/MLSVP/ECC/4.html)
- [സഭാപ്രസംഗി 5](/bible/MLSVP/ECC/5.html)
- [സഭാപ്രസംഗി 6](/bible/MLSVP/ECC/6.html)
- [സഭാപ്രസംഗി 7](/bible/MLSVP/ECC/7.html)
- [സഭാപ്രസംഗി 8](/bible/MLSVP/ECC/8.html)
- [സഭാപ്രസംഗി 9](/bible/MLSVP/ECC/9.html)
- [സഭാപ്രസംഗി 10](/bible/MLSVP/ECC/10.html)
- [സഭാപ്രസംഗി 11](/bible/MLSVP/ECC/11.html)
- [സഭാപ്രസംഗി 12](/bible/MLSVP/ECC/12.html)
