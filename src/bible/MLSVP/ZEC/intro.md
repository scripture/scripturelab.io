## സെഖര്യാവു
- Number of chapters: 14
- Number of verses: 211
## Chapters
- [സെഖര്യാവു 1](/bible/MLSVP/ZEC/1.html)
- [സെഖര്യാവു 2](/bible/MLSVP/ZEC/2.html)
- [സെഖര്യാവു 3](/bible/MLSVP/ZEC/3.html)
- [സെഖര്യാവു 4](/bible/MLSVP/ZEC/4.html)
- [സെഖര്യാവു 5](/bible/MLSVP/ZEC/5.html)
- [സെഖര്യാവു 6](/bible/MLSVP/ZEC/6.html)
- [സെഖര്യാവു 7](/bible/MLSVP/ZEC/7.html)
- [സെഖര്യാവു 8](/bible/MLSVP/ZEC/8.html)
- [സെഖര്യാവു 9](/bible/MLSVP/ZEC/9.html)
- [സെഖര്യാവു 10](/bible/MLSVP/ZEC/10.html)
- [സെഖര്യാവു 11](/bible/MLSVP/ZEC/11.html)
- [സെഖര്യാവു 12](/bible/MLSVP/ZEC/12.html)
- [സെഖര്യാവു 13](/bible/MLSVP/ZEC/13.html)
- [സെഖര്യാവു 14](/bible/MLSVP/ZEC/14.html)
