## ദാനീയേൽ
- Number of chapters: 12
- Number of verses: 357
## Chapters
- [ദാനീയേൽ 1](/bible/MLSVP/DAN/1.html)
- [ദാനീയേൽ 2](/bible/MLSVP/DAN/2.html)
- [ദാനീയേൽ 3](/bible/MLSVP/DAN/3.html)
- [ദാനീയേൽ 4](/bible/MLSVP/DAN/4.html)
- [ദാനീയേൽ 5](/bible/MLSVP/DAN/5.html)
- [ദാനീയേൽ 6](/bible/MLSVP/DAN/6.html)
- [ദാനീയേൽ 7](/bible/MLSVP/DAN/7.html)
- [ദാനീയേൽ 8](/bible/MLSVP/DAN/8.html)
- [ദാനീയേൽ 9](/bible/MLSVP/DAN/9.html)
- [ദാനീയേൽ 10](/bible/MLSVP/DAN/10.html)
- [ദാനീയേൽ 11](/bible/MLSVP/DAN/11.html)
- [ദാനീയേൽ 12](/bible/MLSVP/DAN/12.html)
