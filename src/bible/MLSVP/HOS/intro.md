## ഹോശേയ
- Number of chapters: 14
- Number of verses: 197
## Chapters
- [ഹോശേയ 1](/bible/MLSVP/HOS/1.html)
- [ഹോശേയ 2](/bible/MLSVP/HOS/2.html)
- [ഹോശേയ 3](/bible/MLSVP/HOS/3.html)
- [ഹോശേയ 4](/bible/MLSVP/HOS/4.html)
- [ഹോശേയ 5](/bible/MLSVP/HOS/5.html)
- [ഹോശേയ 6](/bible/MLSVP/HOS/6.html)
- [ഹോശേയ 7](/bible/MLSVP/HOS/7.html)
- [ഹോശേയ 8](/bible/MLSVP/HOS/8.html)
- [ഹോശേയ 9](/bible/MLSVP/HOS/9.html)
- [ഹോശേയ 10](/bible/MLSVP/HOS/10.html)
- [ഹോശേയ 11](/bible/MLSVP/HOS/11.html)
- [ഹോശേയ 12](/bible/MLSVP/HOS/12.html)
- [ഹോശേയ 13](/bible/MLSVP/HOS/13.html)
- [ഹോശേയ 14](/bible/MLSVP/HOS/14.html)
