# 1 തിമൊഥെയൊസ് 2
```
1 എന്നാൽ സകലമനുഷ്യർക്കും നാം സർവ്വഭക്തിയോടും ഘനത്തോടുംകൂടെ സാവധാനതയും സ്വസ്ഥതയുമുള്ള ജീവനം കഴിക്കേണ്ടതിന്നു വിശേഷാൽ രാജാക്കന്മാർക്കും സകലഅധികാരസ്ഥന്മാർക്കും വേണ്ടി
```
```
2 യാചനയും പ്രാർത്ഥനയും പക്ഷവാദവും സ്തോത്രവും ചെയ്യേണം എന്നു ഞാൻ സകലത്തിന്നും മുമ്പെ പ്രബോധിപ്പിക്കുന്നു.
```
```
3 അതു നമ്മുടെ രക്ഷിതാവായ ദൈവത്തിന്റെ സന്നിധിയിൽ നല്ലതും പ്രസാദകരവും ആകുന്നു.
```
```
4 അവൻ സകലമനുഷ്യരും രക്ഷ പ്രാപിപ്പാനും സത്യത്തിന്റെ പരിജ്ഞാനത്തിൽ എത്തുവാനും ഇച്ഛിക്കുന്നു.
```
```
5 ദൈവം ഒരുവനല്ലോ; ദൈവത്തിന്നും മനുഷ്യർക്കും മദ്ധ്യസ്ഥനും ഒരുവൻ:
```
```
6 എല്ലാവർക്കും വേണ്ടി മറുവിലയായി തന്നെത്താൻ കൊടുത്ത മനുഷ്യനായ ക്രിസ്തുയേശു തന്നേ.
```
```
7 തക്കസമയത്തു അറിയിക്കേണ്ടിയ ഈ സാക്ഷ്യത്തിന്നായി ഞാൻ പ്രസംഗിയും അപ്പൊസ്തലനുമായി -- ഭോഷ്കല്ല, പരമാർത്ഥം തന്നേ പറയുന്നു -- ജാതികളെ വിശ്വാസവും സത്യവും ഉപദേശിപ്പാൻ നിയമിക്കപ്പെട്ടിരിക്കുന്നു.
```
```
8 ആകയാൽ പുരുഷന്മാർ എല്ലാടത്തും കോപവും വാഗ്വാദവും വിട്ടകന്നു വിശുദ്ധകൈകളെ ഉയർത്തി പ്രാർത്ഥിക്കേണം എന്നു ഞാൻ ആഗ്രഹിക്കുന്നു.
```
```
9 അവ്വണ്ണം സ്ത്രീകളും യോഗ്യമായ വസ്ത്രം ധരിച്ചു ലജ്ജാശീലത്തോടും സുബോധത്തോടുംകൂടെ തങ്ങളെ അലങ്കരിക്കേണം.
```
```
10 പിന്നിയ തലമുടി, പൊന്നു, മുത്തു, വിലയേറിയ വസ്ത്രം എന്നിവകൊണ്ടല്ല, ദൈവഭക്തിയെ സ്വീകരിക്കുന്ന സ്ത്രീകൾക്കു ഉചിതമാകുംവണ്ണം സൽപ്രവൃത്തികളെക്കൊണ്ടത്രേ അലങ്കരിക്കേണ്ടതു.
```
```
11 സ്ത്രീ മൌനമായിരുന്നു പൂർണ്ണാനുസരണത്തോടും കൂടെ പഠിക്കട്ടെ.
```
```
12 മൌനമായിരിപ്പാൻ അല്ലാതെ ഉപദേശിപ്പാനോ പുരുഷന്റെമേൽ അധികാരം നടത്തുവാനോ ഞാൻ സ്ത്രീയെ അനുവദിക്കുന്നില്ല.
```
```
13 ആദാം ആദ്യം നിർമ്മിക്കപ്പെട്ടു, പിന്നെ ഹവ്വ;
```
```
14 ആദാം അല്ല, സ്ത്രീ അത്രേ വഞ്ചിക്കപ്പെട്ടു ലംഘനത്തിൽ അകപ്പെട്ടതു.
```
```
15 എന്നാൽ വിശ്വാസത്തിലും സ്നേഹത്തിലും വിശുദ്ധീകരണത്തിലും സുബോധത്തോടെ പാർക്കുന്നു എങ്കിൽ അവൾ മക്കളെ പ്രസവിച്ചു രക്ഷ പ്രാപിക്കും
```
