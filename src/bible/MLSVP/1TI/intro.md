## 1 തിമൊഥെയൊസ്
- Number of chapters: 6
- Number of verses: 114
## Chapters
- [1 തിമൊഥെയൊസ് 1](/bible/MLSVP/1TI/1.html)
- [1 തിമൊഥെയൊസ് 2](/bible/MLSVP/1TI/2.html)
- [1 തിമൊഥെയൊസ് 3](/bible/MLSVP/1TI/3.html)
- [1 തിമൊഥെയൊസ് 4](/bible/MLSVP/1TI/4.html)
- [1 തിമൊഥെയൊസ് 5](/bible/MLSVP/1TI/5.html)
- [1 തിമൊഥെയൊസ് 6](/bible/MLSVP/1TI/6.html)
