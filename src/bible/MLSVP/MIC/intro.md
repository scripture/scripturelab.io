## മീഖാ
- Number of chapters: 7
- Number of verses: 105
## Chapters
- [മീഖാ 1](/bible/MLSVP/MIC/1.html)
- [മീഖാ 2](/bible/MLSVP/MIC/2.html)
- [മീഖാ 3](/bible/MLSVP/MIC/3.html)
- [മീഖാ 4](/bible/MLSVP/MIC/4.html)
- [മീഖാ 5](/bible/MLSVP/MIC/5.html)
- [മീഖാ 6](/bible/MLSVP/MIC/6.html)
- [മീഖാ 7](/bible/MLSVP/MIC/7.html)
