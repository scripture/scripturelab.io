# സദൃശവാക്യങ്ങൾ 19
```
1 വികടാധരം ഉള്ള മൂഢനെക്കാൾ
```
```
2 പരിജ്ഞാനമില്ലാത്ത മനസ്സു നന്നല്ല;
```
```
3 മനുഷ്യന്റെ ഭോഷത്വം അവന്റെ വഴിയെ മറിച്ചുകളയുന്നു;
```
```
4 സമ്പത്തു സ്നേഹിതന്മാരെ വർദ്ധിപ്പിക്കുന്നു;
```
```
5 കള്ളസ്സാക്ഷിക്കു ശിക്ഷ വരാതിരിക്കയില്ല;
```
```
6 പ്രഭുവിന്റെ പ്രീതി സമ്പാദിപ്പാൻ പലരും നോക്കുന്നു;
```
```
7 ദരിദ്രന്റെ സഹോദരന്മാരെല്ലാം അവനെ പകെക്കുന്നു;
```
```
8 ബുദ്ധി സമ്പാദിക്കുന്നവൻ തന്റെ പ്രാണനെ സ്നേഹിക്കുന്നു;
```
```
9 കള്ളസ്സാക്ഷിക്കു ശിക്ഷ വരാതിരിക്കയില്ല;
```
```
10 സുഖജീവനം ഭോഷന്നു യോഗ്യമല്ല;
```
```
11 വിവേകബുദ്ധിയാൽ മനുഷ്യന്നു ദീർഘക്ഷമവരുന്നു;
```
```
12 രാജാവിന്റെ ക്രോധം സിംഹഗർജ്ജനത്തിന്നു തുല്യം;
```
```
13 മൂഢനായ മകൻ അപ്പന്നു നിർഭാഗ്യം;
```
```
14 ഭവനവും സമ്പത്തും പിതാക്കന്മാർ വെച്ചേക്കുന്ന അവകാശം;
```
```
15 മടി ഗാഢനിദ്രയിൽ വീഴിക്കുന്നു;
```
```
16 കല്പന പ്രമാണിക്കുന്നവൻ പ്രാണനെ കാക്കുന്നു;
```
```
17 എളിയവനോടു കൃപ കാട്ടുന്നവൻ യഹോവെക്കു വായ്പ കൊടുക്കുന്നു;
```
```
18 പ്രത്യാശയുള്ളേടത്തോളം നിന്റെ മകനെ ശിക്ഷിക്ക;
```
```
19 മുൻകോപി പിഴ കൊടുക്കേണ്ടിവരും;
```
```
20 പിന്നത്തേതിൽ നീ ജ്ഞാനിയാകേണ്ടതിന്നു
```
```
21 മനുഷ്യന്റെ ഹൃദയത്തിൽ പല വിചാരങ്ങളും ഉണ്ടു;
```
```
22 മനുഷ്യൻ തന്റെ മനസ്സുപോലെ ദയ കാണിക്കും;
```
```
23 യഹോവാഭക്തി ജീവഹേതുകമാകുന്നു;
```
```
24 മടിയൻ തന്റെ കൈ തളികയിൽ പൂത്തുന്നു;
```
```
25 പരിഹാസിയെ അടിച്ചാൽ അല്പബുദ്ധി വിവേകം പഠിക്കും;
```
```
26 അപ്പനെ ഹേമിക്കയും അമ്മയെ ഓടിച്ചുകളകയും ചെയ്യുന്നവൻ
```
```
27 മകനേ, പരിജ്ഞാനത്തിന്റെ വചനങ്ങളെ
```
```
28 നിസ്സാരസാക്ഷി ന്യായത്തെ പരിഹസിക്കുന്നു;
```
```
29 പരിഹാസികൾക്കായി ശിക്ഷാവിധിയും
```
