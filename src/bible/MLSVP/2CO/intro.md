## 2 കൊരിന്ത്യർ
- Number of chapters: 13
- Number of verses: 257
## Chapters
- [2 കൊരിന്ത്യർ 1](/bible/MLSVP/2CO/1.html)
- [2 കൊരിന്ത്യർ 2](/bible/MLSVP/2CO/2.html)
- [2 കൊരിന്ത്യർ 3](/bible/MLSVP/2CO/3.html)
- [2 കൊരിന്ത്യർ 4](/bible/MLSVP/2CO/4.html)
- [2 കൊരിന്ത്യർ 5](/bible/MLSVP/2CO/5.html)
- [2 കൊരിന്ത്യർ 6](/bible/MLSVP/2CO/6.html)
- [2 കൊരിന്ത്യർ 7](/bible/MLSVP/2CO/7.html)
- [2 കൊരിന്ത്യർ 8](/bible/MLSVP/2CO/8.html)
- [2 കൊരിന്ത്യർ 9](/bible/MLSVP/2CO/9.html)
- [2 കൊരിന്ത്യർ 10](/bible/MLSVP/2CO/10.html)
- [2 കൊരിന്ത്യർ 11](/bible/MLSVP/2CO/11.html)
- [2 കൊരിന്ത്യർ 12](/bible/MLSVP/2CO/12.html)
- [2 കൊരിന്ത്യർ 13](/bible/MLSVP/2CO/13.html)
