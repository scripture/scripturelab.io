# ഇയ്യോബ് 41
```
1 മഹാനക്രത്തെ ചൂണ്ടലിട്ടു പിടിക്കാമോ?
```
```
2 അതിന്റെ മൂക്കിൽ കയറു കോർക്കാമോ?
```
```
3 അതു നിന്നോടു ഏറിയ യാചന കഴിക്കുമോ?
```
```
4 അതിനെ എന്നും ദാസനാക്കിക്കൊള്ളേണ്ടതിന്നു
```
```
5 പക്ഷിയോടു എന്നപോലെ നീ അതിനോടു കളിക്കുമോ?
```
```
6 മീൻപിടിക്കൂറ്റുകാർ അതിനെക്കൊണ്ടു വ്യാപാരം ചെയ്യുമോ?
```
```
7 നിനക്കു അതിന്റെ തോലിൽ നിറെച്ചു അസ്ത്രവും
```
```
8 അതിനെ ഒന്നു തൊടുക; പോർ തിട്ടം എന്നു ഓർത്തുകൊൾക;
```
```
9 അവന്റെ ആശെക്കു ഭംഗംവരുന്നു;
```
```
10 അതിനെ ഇളക്കുവാൻ തക്ക ശൂരനില്ല;
```
```
11 ഞാൻ മടക്കിക്കൊടുക്കേണ്ടതിന്നു എനിക്കു മുമ്പുകൂട്ടി തന്നതാർ?
```
```
12 അതിന്റെ അവയവങ്ങളെയും മഹാശക്തിയെയും
```
```
13 അതിന്റെ പുറങ്കുപ്പായം ഊരാകുന്നവനാർ?
```
```
14 അതിന്റെ മുഖത്തെ കതകു ആർ തുറക്കും?
```
```
15 ചെതുമ്പൽനിര അതിന്റെ ഡംഭമാകുന്നു;
```
```
16 അതു ഒന്നോടൊന്നു പറ്റിയിരിക്കുന്നു; ഇടയിൽ കാറ്റുകടക്കയില്ല.
```
```
17 ഒന്നോടൊന്നു ചേർന്നിരിക്കുന്നു;
```
```
18 അതു തുമ്മുമ്പോൾ വെളിച്ചം മിന്നുന്നു;
```
```
19 അതിന്റെ വായിൽനിന്നു തീപ്പന്തങ്ങൾ പുറപ്പെടുകയും
```
```
20 തിളെക്കുന്ന കലത്തിൽനിന്നും കത്തുന്ന പോട്ടപ്പുല്ലിൽനിന്നും
```
```
21 അതിന്റെ ശ്വാസം കനൽ ജ്വലിപ്പിക്കുന്നു;
```
```
22 അതിന്റെ കഴുത്തിൽ ബലം വസിക്കുന്നു;
```
```
23 അതിന്റെ മാംസദശകൾ തമ്മിൽ പറ്റിയിരിക്കുന്നു;
```
```
24 അതിന്റെ ഹൃദയം കല്ലുപോലെ ഉറപ്പുള്ളതു;
```
```
25 അതു പൊങ്ങുമ്പോൾ ബലശാലികൾ പേടിക്കുന്നു;
```
```
26 വാൾകൊണ്ടു അതിനെ എതിർക്കുന്നതു അസാദ്ധ്യം;
```
```
27 ഇരിമ്പിനെ അതു വൈക്കോൽപോലെയും
```
```
28 അസ്ത്രം അതിനെ ഓടിക്കയില്ല;
```
```
29 ഗദ അതിന്നു താളടിപോലെ തോന്നുന്നു;
```
```
30 അതിന്റെ അധോഭാഗം മൂർച്ചയുള്ള ഓട്ടുകഷണംപോലെയാകുന്നു;
```
```
31 കലത്തെപ്പോലെ അതു ആഴിയെ തിളെപ്പിക്കുന്നു;
```
```
32 അതിന്റെ പിന്നാലെ ഒരു പാത മിന്നുന്നു;
```
```
33 ഭൂമിയിൽ അതിന്നു തുല്യമായിട്ടൊന്നും ഇല്ല;
```
```
34 അതു ഉന്നതമായുള്ളതിനെയൊക്കെയും നോക്കിക്കാണുന്നു;
```
