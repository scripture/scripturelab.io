## 1 കൊരിന്ത്യർ
- Number of chapters: 16
- Number of verses: 437
## Chapters
- [1 കൊരിന്ത്യർ 1](/bible/MLSVP/1CO/1.html)
- [1 കൊരിന്ത്യർ 2](/bible/MLSVP/1CO/2.html)
- [1 കൊരിന്ത്യർ 3](/bible/MLSVP/1CO/3.html)
- [1 കൊരിന്ത്യർ 4](/bible/MLSVP/1CO/4.html)
- [1 കൊരിന്ത്യർ 5](/bible/MLSVP/1CO/5.html)
- [1 കൊരിന്ത്യർ 6](/bible/MLSVP/1CO/6.html)
- [1 കൊരിന്ത്യർ 7](/bible/MLSVP/1CO/7.html)
- [1 കൊരിന്ത്യർ 8](/bible/MLSVP/1CO/8.html)
- [1 കൊരിന്ത്യർ 9](/bible/MLSVP/1CO/9.html)
- [1 കൊരിന്ത്യർ 10](/bible/MLSVP/1CO/10.html)
- [1 കൊരിന്ത്യർ 11](/bible/MLSVP/1CO/11.html)
- [1 കൊരിന്ത്യർ 12](/bible/MLSVP/1CO/12.html)
- [1 കൊരിന്ത്യർ 13](/bible/MLSVP/1CO/13.html)
- [1 കൊരിന്ത്യർ 14](/bible/MLSVP/1CO/14.html)
- [1 കൊരിന്ത്യർ 15](/bible/MLSVP/1CO/15.html)
- [1 കൊരിന്ത്യർ 16](/bible/MLSVP/1CO/16.html)
