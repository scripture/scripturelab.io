## ഉത്തമഗീതം
- Number of chapters: 8
- Number of verses: 117
## Chapters
- [ഉത്തമഗീതം 1](/bible/MLSVP/SNG/1.html)
- [ഉത്തമഗീതം 2](/bible/MLSVP/SNG/2.html)
- [ഉത്തമഗീതം 3](/bible/MLSVP/SNG/3.html)
- [ഉത്തമഗീതം 4](/bible/MLSVP/SNG/4.html)
- [ഉത്തമഗീതം 5](/bible/MLSVP/SNG/5.html)
- [ഉത്തമഗീതം 6](/bible/MLSVP/SNG/6.html)
- [ഉത്തമഗീതം 7](/bible/MLSVP/SNG/7.html)
- [ഉത്തമഗീതം 8](/bible/MLSVP/SNG/8.html)
