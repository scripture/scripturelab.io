## നെഹെമ്യാവു
- Number of chapters: 13
- Number of verses: 406
## Chapters
- [നെഹെമ്യാവു 1](/bible/MLSVP/NEH/1.html)
- [നെഹെമ്യാവു 2](/bible/MLSVP/NEH/2.html)
- [നെഹെമ്യാവു 3](/bible/MLSVP/NEH/3.html)
- [നെഹെമ്യാവു 4](/bible/MLSVP/NEH/4.html)
- [നെഹെമ്യാവു 5](/bible/MLSVP/NEH/5.html)
- [നെഹെമ്യാവു 6](/bible/MLSVP/NEH/6.html)
- [നെഹെമ്യാവു 7](/bible/MLSVP/NEH/7.html)
- [നെഹെമ്യാവു 8](/bible/MLSVP/NEH/8.html)
- [നെഹെമ്യാവു 9](/bible/MLSVP/NEH/9.html)
- [നെഹെമ്യാവു 10](/bible/MLSVP/NEH/10.html)
- [നെഹെമ്യാവു 11](/bible/MLSVP/NEH/11.html)
- [നെഹെമ്യാവു 12](/bible/MLSVP/NEH/12.html)
- [നെഹെമ്യാവു 13](/bible/MLSVP/NEH/13.html)
