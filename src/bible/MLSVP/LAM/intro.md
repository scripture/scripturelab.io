## വിലാപങ്ങൾ
- Number of chapters: 5
- Number of verses: 154
## Chapters
- [വിലാപങ്ങൾ 1](/bible/MLSVP/LAM/1.html)
- [വിലാപങ്ങൾ 2](/bible/MLSVP/LAM/2.html)
- [വിലാപങ്ങൾ 3](/bible/MLSVP/LAM/3.html)
- [വിലാപങ്ങൾ 4](/bible/MLSVP/LAM/4.html)
- [വിലാപങ്ങൾ 5](/bible/MLSVP/LAM/5.html)
