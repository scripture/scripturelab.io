# സങ്കീർത്തനങ്ങൾ 31
```
1 യഹോവേ, ഞാൻ നിന്നെ ശരണം പ്രാപിക്കുന്നു;
```
```
2 നിന്റെ ചെവി എങ്കലേക്കു ചായിച്ചു എന്നെ വേഗം വിടുവിക്കേണമേ.
```
```
3 നീ എന്റെ പാറയും എന്റെ കോട്ടയുമല്ലോ;
```
```
4 അവർ എനിക്കായി ഒളിച്ചുവെച്ചിരിക്കുന്ന വലയിൽനിന്നു എന്നെ വിടുവിക്കേണമേ;
```
```
5 നിന്റെ കയ്യിൽ ഞാൻ എന്റെ ആത്മാവിനെ ഭരമേല്പിക്കുന്നു;
```
```
6 മിത്ഥ്യാമൂർത്തികളെ സേവിക്കുന്നവരെ ഞാൻ പകെക്കുന്നു;
```
```
7 ഞാൻ നിന്റെ ദയയിൽ ആനന്ദിച്ചു സന്തോഷിക്കുന്നു;
```
```
8 ശത്രുവിന്റെ കയ്യിൽ നീ എന്നെ ഏല്പിച്ചിട്ടില്ല;
```
```
9 യഹോവേ, എന്നോടു കൃപയുണ്ടാകേണമേ; ഞാൻ കഷ്ടത്തിലായിരിക്കുന്നു;
```
```
10 എന്റെ ആയുസ്സു ദുഃഖംകൊണ്ടും എന്റെ സംവത്സരങ്ങൾ നെടുവീർപ്പുകൊണ്ടും കഴിഞ്ഞുപോയിരിക്കുന്നു;
```
```
11 എന്റെ സകലവൈരികളാലും ഞാൻ നിന്ദിതനായിത്തീർന്നു;
```
```
12 മരിച്ചുപോയവനെപ്പോലെ എന്നെ മറന്നുകളഞ്ഞിരിക്കുന്നു;
```
```
13 ചുറ്റും ഭീതി എന്ന അപശ്രുതി ഞാൻ പലരുടെയും വായിൽനിന്നു കേട്ടിരിക്കുന്നു;
```
```
14 എങ്കിലും യഹോവേ, ഞാൻ നിന്നിൽ ആശ്രയിച്ചു;
```
```
15 എന്റെ കാലഗതികൾ നിന്റെ കയ്യിൽ ഇരിക്കുന്നു;
```
```
16 അടിയന്റെമേൽ തിരുമുഖം പ്രകാശിപ്പിക്കേണമേ;
```
```
17 യഹോവേ, നിന്നെ വിളിച്ചപേക്ഷിച്ചിരിക്കകൊണ്ടു ഞാൻ ലജ്ജിച്ചുപോകരുതേ;
```
```
18 നീതിമാന്നു വിരോധമായി ഡംഭത്തോടും നിന്ദയോടും കൂടെ
```
```
19 നിന്റെ ഭക്തന്മാർക്കു വേണ്ടി നീ സംഗ്രഹിച്ചതും
```
```
20 നീ അവരെ മനുഷ്യരുടെ കൂട്ടുകെട്ടിൽനിന്നു വിടുവിച്ചു
```
```
21 യഹോവ വാഴ്ത്തപ്പെട്ടവൻ; അവൻ ഉറപ്പുള്ള പട്ടണത്തിൽ
```
```
22 ഞാൻ നിന്റെ ദൃഷ്ടിയിൽനിന്നു ഛേദിക്കപ്പെട്ടുപോയി എന്നു ഞാൻ എന്റെ പരിഭ്രമത്തിൽ പറഞ്ഞു;
```
```
23 യഹോവയുടെ സകലവിശുദ്ധന്മാരുമായുള്ളോരേ, അവനെ സ്നേഹിപ്പിൻ;
```
```
24 യഹോവയിൽ പ്രത്യാശയുള്ള ഏവരുമേ, ധൈര്യപ്പെട്ടിരിപ്പിൻ;
```
