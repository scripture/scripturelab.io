# സങ്കീർത്തനങ്ങൾ 139
```
1 യഹോവേ, നീ എന്നെ ശോധന ചെയ്തു അറിഞ്ഞിരിക്കുന്നു;
```
```
2 ഞാൻ ഇരിക്കുന്നതും എഴുന്നേല്ക്കുന്നതും നീ അറിയുന്നു.
```
```
3 എന്റെ നടപ്പും കിടപ്പും നീ ശോധന ചെയ്യുന്നു;
```
```
4 യഹോവേ, നീ മുഴുവനും അറിയാതെ ഒരു വാക്കും എന്റെ നാവിന്മേൽ ഇല്ല.
```
```
5 നീ മുമ്പും പിമ്പും എന്നെ അടെച്ചു
```
```
6 ഈ പരിജ്ഞാനം എനിക്കു അത്യത്ഭുതമാകുന്നു;
```
```
7 നിന്റെ ആത്മാവിനെ ഒളിച്ചു ഞാൻ എവിടേക്കു പോകും?
```
```
8 ഞാൻ സ്വർഗ്ഗത്തിൽ കയറിയാൽ നീ അവിടെ ഉണ്ടു;
```
```
9 ഞാൻ ഉഷസ്സിൻ ചിറകു ധരിച്ചു,
```
```
10 അവിടെയും നിന്റെ കൈ എന്നെ നടത്തും;
```
```
11 ഇരുട്ടു എന്നെ മൂടിക്കളയട്ടെ;
```
```
12 ഇരുട്ടുപോലും നിനക്കു മറവായിരിക്കയില്ല;
```
```
13 നീയല്ലോ എന്റെ അന്തരംഗങ്ങളെ നിർമ്മിച്ചതു;
```
```
14 ഭയങ്കരവും അതിശയവുമായി എന്നെ സൃഷ്ടിച്ചിരിക്കയാൽ
```
```
15 ഞാൻ രഹസ്യത്തിൽ ഉണ്ടാക്കപ്പെടുകയും
```
```
16 ഞാൻ പിണ്ഡാകാരമായിരുന്നപ്പോൾ നിന്റെ കണ്ണു എന്നെ കണ്ടു;
```
```
17 ദൈവമേ, നിന്റെ വിചാരങ്ങൾ എനിക്കു എത്ര ഘനമായവ!
```
```
18 അവയെ എണ്ണിയാൽ മണലിനെക്കാൾ അധികം;
```
```
19 ദൈവമേ, നീ ദുഷ്ടനെ നിഗ്രഹിച്ചെങ്കിൽ കൊള്ളായിരുന്നു;
```
```
20 അവർ ദ്രോഹമായി നിന്നെക്കുറിച്ചു സംസാരിക്കുന്നു;
```
```
21 യഹോവേ, നിന്നെ പകെക്കുന്നവരെ ഞാൻ പകക്കേണ്ടതല്ലയോ?
```
```
22 ഞാൻ പൂർണ്ണദ്വേഷത്തോടെ അവരെ ദ്വേഷിക്കുന്നു;
```
```
23 ദൈവമേ, എന്നെ ശോധന ചെയ്തു എന്റെ ഹൃദയത്തെ അറിയേണമേ;
```
```
24 വ്യസനത്തിന്നുള്ള മാർഗ്ഗം എന്നിൽ ഉണ്ടോ എന്നു നോക്കി,
```
