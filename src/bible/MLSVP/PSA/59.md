# സങ്കീർത്തനങ്ങൾ 59
```
1 എന്റെ ദൈവമേ, എന്റെ ശത്രുക്കളുടെ കയ്യിൽനിന്നു എന്നെ വിടുവിക്കേണമേ;
```
```
2 നീതികേടു പ്രവർത്തിക്കുന്നവരുടെ കയ്യിൽ നിന്നു എന്നെ വിടുവിച്ചു
```
```
3 ഇതാ, അവർ എന്റെ പ്രാണന്നായി പതിയിരിക്കുന്നു;
```
```
4 എന്റെ പക്കൽ അകൃത്യം ഇല്ലാതെ അവർ ഓടി ഒരുങ്ങുന്നു;
```
```
5 സൈന്യങ്ങളുടെ ദൈവമായ യഹോവേ, യിസ്രായേലിന്റെ ദൈവമേ,
```
```
6 സന്ധ്യാസമയത്തു അവർ മടങ്ങിവരുന്നു;
```
```
7 അവർ തങ്ങളുടെ വായ്കൊണ്ടു ശകാരിക്കുന്നു;
```
```
8 എങ്കിലും യഹോവേ, നീ അവരെച്ചൊല്ലി ചിരിക്കും;
```
```
9 എന്റെ ബലമായുള്ളോവേ, ഞാൻ നിന്നെ കാത്തിരിക്കും;
```
```
10 എന്റെ ദൈവം തന്റെ ദയയാൽ എന്നെ എതിരേല്ക്കും;
```
```
11 അവരെ കൊന്നുകളയരുതേ; എന്റെ ജനം മറക്കാതിരിക്കേണ്ടതിന്നു തന്നേ;
```
```
12 അവരുടെ വായിലെ പാപവും അധരങ്ങളിലെ വാക്കുകളുംനിമിത്തവും
```
```
13 കോപത്തോടെ അവരെ സംഹരിക്കേണമേ;
```
```
14 സന്ധ്യാസമയത്തു അവർ മടങ്ങിവരുന്നു;
```
```
15 അവർ ആഹാരത്തിന്നായി ഉഴന്നുനടക്കുന്നു;
```
```
16 ഞാനോ നിന്റെ ബലത്തെക്കുറിച്ചു പാടും;
```
```
17 എന്റെ ബലമായുള്ളോവേ, ഞാൻ നിനക്കു സ്തുതിപാടും;
```
