# സങ്കീർത്തനങ്ങൾ 97
```
1 യഹോവ വാഴുന്നു; ഭൂമി ഘോഷിച്ചാനന്ദിക്കട്ടെ;
```
```
2 മേഘവും അന്ധകാരവും അവന്റെ ചുറ്റും ഇരിക്കുന്നു;
```
```
3 തീ അവന്നു മുമ്പായി പോകുന്നു;
```
```
4 അവന്റെ മിന്നലുകൾ ഭൂതലത്തെ പ്രകാശിപ്പിക്കുന്നു;
```
```
5 യഹോവയുടെ സന്നിധിയിൽ, സർവ്വഭൂമിയുടെയും കർത്താവിന്റെ സന്നിധിയിൽ,
```
```
6 ആകാശം അവന്റെ നീതിയെ പ്രസിദ്ധമാക്കുന്നു;
```
```
7 വിഗ്രഹങ്ങളെ സേവിക്കയും ബിംബങ്ങളിൽ പ്രശംസിക്കയും ചെയുന്നവരൊക്കെയും ലജ്ജിച്ചുപോകും;
```
```
8 സീയോൻ കേട്ടു സന്തോഷിക്കുന്നു;
```
```
9 യഹോവേ, നീ സർവ്വഭൂമിക്കും മീതെ അത്യുന്നതൻ;
```
```
10 യഹോവയെ സ്നേഹിക്കുന്നവരേ, ദോഷത്തെ വെറുപ്പിൻ;
```
```
11 നീതിമാന്നു പ്രകാശവും പരമാർത്ഥഹൃദയമുള്ളവർക്കു സന്തോഷവും ഉദിക്കും.
```
```
12 നീതിമാന്മാരേ, യഹോവയിൽ സന്തോഷിപ്പിൻ;
```
