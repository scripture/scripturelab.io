## കൊലൊസ്സ്യർ
- Number of chapters: 4
- Number of verses: 95
## Chapters
- [കൊലൊസ്സ്യർ 1](/bible/MLSVP/COL/1.html)
- [കൊലൊസ്സ്യർ 2](/bible/MLSVP/COL/2.html)
- [കൊലൊസ്സ്യർ 3](/bible/MLSVP/COL/3.html)
- [കൊലൊസ്സ്യർ 4](/bible/MLSVP/COL/4.html)
