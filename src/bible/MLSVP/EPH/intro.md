## എഫെസ്യർ
- Number of chapters: 6
- Number of verses: 155
## Chapters
- [എഫെസ്യർ 1](/bible/MLSVP/EPH/1.html)
- [എഫെസ്യർ 2](/bible/MLSVP/EPH/2.html)
- [എഫെസ്യർ 3](/bible/MLSVP/EPH/3.html)
- [എഫെസ്യർ 4](/bible/MLSVP/EPH/4.html)
- [എഫെസ്യർ 5](/bible/MLSVP/EPH/5.html)
- [എഫെസ്യർ 6](/bible/MLSVP/EPH/6.html)
