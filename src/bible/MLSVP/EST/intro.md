## എസ്ഥേർ
- Number of chapters: 10
- Number of verses: 167
## Chapters
- [എസ്ഥേർ 1](/bible/MLSVP/EST/1.html)
- [എസ്ഥേർ 2](/bible/MLSVP/EST/2.html)
- [എസ്ഥേർ 3](/bible/MLSVP/EST/3.html)
- [എസ്ഥേർ 4](/bible/MLSVP/EST/4.html)
- [എസ്ഥേർ 5](/bible/MLSVP/EST/5.html)
- [എസ്ഥേർ 6](/bible/MLSVP/EST/6.html)
- [എസ്ഥേർ 7](/bible/MLSVP/EST/7.html)
- [എസ്ഥേർ 8](/bible/MLSVP/EST/8.html)
- [എസ്ഥേർ 9](/bible/MLSVP/EST/9.html)
- [എസ്ഥേർ 10](/bible/MLSVP/EST/10.html)
