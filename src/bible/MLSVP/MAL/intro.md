## മലാഖി
- Number of chapters: 4
- Number of verses: 55
## Chapters
- [മലാഖി 1](/bible/MLSVP/MAL/1.html)
- [മലാഖി 2](/bible/MLSVP/MAL/2.html)
- [മലാഖി 3](/bible/MLSVP/MAL/3.html)
- [മലാഖി 4](/bible/MLSVP/MAL/4.html)
