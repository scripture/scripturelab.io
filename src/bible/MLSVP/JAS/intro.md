## യാക്കോബ്
- Number of chapters: 5
- Number of verses: 108
## Chapters
- [യാക്കോബ് 1](/bible/MLSVP/JAS/1.html)
- [യാക്കോബ് 2](/bible/MLSVP/JAS/2.html)
- [യാക്കോബ് 3](/bible/MLSVP/JAS/3.html)
- [യാക്കോബ് 4](/bible/MLSVP/JAS/4.html)
- [യാക്കോബ് 5](/bible/MLSVP/JAS/5.html)
