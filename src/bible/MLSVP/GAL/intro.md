## ഗലാത്യർ
- Number of chapters: 6
- Number of verses: 149
## Chapters
- [ഗലാത്യർ 1](/bible/MLSVP/GAL/1.html)
- [ഗലാത്യർ 2](/bible/MLSVP/GAL/2.html)
- [ഗലാത്യർ 3](/bible/MLSVP/GAL/3.html)
- [ഗലാത്യർ 4](/bible/MLSVP/GAL/4.html)
- [ഗലാത്യർ 5](/bible/MLSVP/GAL/5.html)
- [ഗലാത്യർ 6](/bible/MLSVP/GAL/6.html)
