## எஸ்றா
- Number of chapters: 10
- Number of verses: 280
## Chapters
- [எஸ்றா 1](/bible/TOVBSI/EZR/1.html)
- [எஸ்றா 2](/bible/TOVBSI/EZR/2.html)
- [எஸ்றா 3](/bible/TOVBSI/EZR/3.html)
- [எஸ்றா 4](/bible/TOVBSI/EZR/4.html)
- [எஸ்றா 5](/bible/TOVBSI/EZR/5.html)
- [எஸ்றா 6](/bible/TOVBSI/EZR/6.html)
- [எஸ்றா 7](/bible/TOVBSI/EZR/7.html)
- [எஸ்றா 8](/bible/TOVBSI/EZR/8.html)
- [எஸ்றா 9](/bible/TOVBSI/EZR/9.html)
- [எஸ்றா 10](/bible/TOVBSI/EZR/10.html)
