## ஓசியா
- Number of chapters: 14
- Number of verses: 197
## Chapters
- [ஓசியா 1](/bible/TOVBSI/HOS/1.html)
- [ஓசியா 2](/bible/TOVBSI/HOS/2.html)
- [ஓசியா 3](/bible/TOVBSI/HOS/3.html)
- [ஓசியா 4](/bible/TOVBSI/HOS/4.html)
- [ஓசியா 5](/bible/TOVBSI/HOS/5.html)
- [ஓசியா 6](/bible/TOVBSI/HOS/6.html)
- [ஓசியா 7](/bible/TOVBSI/HOS/7.html)
- [ஓசியா 8](/bible/TOVBSI/HOS/8.html)
- [ஓசியா 9](/bible/TOVBSI/HOS/9.html)
- [ஓசியா 10](/bible/TOVBSI/HOS/10.html)
- [ஓசியா 11](/bible/TOVBSI/HOS/11.html)
- [ஓசியா 12](/bible/TOVBSI/HOS/12.html)
- [ஓசியா 13](/bible/TOVBSI/HOS/13.html)
- [ஓசியா 14](/bible/TOVBSI/HOS/14.html)
