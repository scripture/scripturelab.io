## 2 கொரிந்தியர்
- Number of chapters: 13
- Number of verses: 257
## Chapters
- [2 கொரிந்தியர் 1](/bible/TOVBSI/2CO/1.html)
- [2 கொரிந்தியர் 2](/bible/TOVBSI/2CO/2.html)
- [2 கொரிந்தியர் 3](/bible/TOVBSI/2CO/3.html)
- [2 கொரிந்தியர் 4](/bible/TOVBSI/2CO/4.html)
- [2 கொரிந்தியர் 5](/bible/TOVBSI/2CO/5.html)
- [2 கொரிந்தியர் 6](/bible/TOVBSI/2CO/6.html)
- [2 கொரிந்தியர் 7](/bible/TOVBSI/2CO/7.html)
- [2 கொரிந்தியர் 8](/bible/TOVBSI/2CO/8.html)
- [2 கொரிந்தியர் 9](/bible/TOVBSI/2CO/9.html)
- [2 கொரிந்தியர் 10](/bible/TOVBSI/2CO/10.html)
- [2 கொரிந்தியர் 11](/bible/TOVBSI/2CO/11.html)
- [2 கொரிந்தியர் 12](/bible/TOVBSI/2CO/12.html)
- [2 கொரிந்தியர் 13](/bible/TOVBSI/2CO/13.html)
