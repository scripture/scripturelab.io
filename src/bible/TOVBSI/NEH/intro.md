## நெகேமியா
- Number of chapters: 13
- Number of verses: 406
## Chapters
- [நெகேமியா 1](/bible/TOVBSI/NEH/1.html)
- [நெகேமியா 2](/bible/TOVBSI/NEH/2.html)
- [நெகேமியா 3](/bible/TOVBSI/NEH/3.html)
- [நெகேமியா 4](/bible/TOVBSI/NEH/4.html)
- [நெகேமியா 5](/bible/TOVBSI/NEH/5.html)
- [நெகேமியா 6](/bible/TOVBSI/NEH/6.html)
- [நெகேமியா 7](/bible/TOVBSI/NEH/7.html)
- [நெகேமியா 8](/bible/TOVBSI/NEH/8.html)
- [நெகேமியா 9](/bible/TOVBSI/NEH/9.html)
- [நெகேமியா 10](/bible/TOVBSI/NEH/10.html)
- [நெகேமியா 11](/bible/TOVBSI/NEH/11.html)
- [நெகேமியா 12](/bible/TOVBSI/NEH/12.html)
- [நெகேமியா 13](/bible/TOVBSI/NEH/13.html)
