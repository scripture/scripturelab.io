## தானியேல்
- Number of chapters: 12
- Number of verses: 357
## Chapters
- [தானியேல் 1](/bible/TOVBSI/DAN/1.html)
- [தானியேல் 2](/bible/TOVBSI/DAN/2.html)
- [தானியேல் 3](/bible/TOVBSI/DAN/3.html)
- [தானியேல் 4](/bible/TOVBSI/DAN/4.html)
- [தானியேல் 5](/bible/TOVBSI/DAN/5.html)
- [தானியேல் 6](/bible/TOVBSI/DAN/6.html)
- [தானியேல் 7](/bible/TOVBSI/DAN/7.html)
- [தானியேல் 8](/bible/TOVBSI/DAN/8.html)
- [தானியேல் 9](/bible/TOVBSI/DAN/9.html)
- [தானியேல் 10](/bible/TOVBSI/DAN/10.html)
- [தானியேல் 11](/bible/TOVBSI/DAN/11.html)
- [தானியேல் 12](/bible/TOVBSI/DAN/12.html)
