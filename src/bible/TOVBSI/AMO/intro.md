## ஆமோஸ்
- Number of chapters: 9
- Number of verses: 146
## Chapters
- [ஆமோஸ் 1](/bible/TOVBSI/AMO/1.html)
- [ஆமோஸ் 2](/bible/TOVBSI/AMO/2.html)
- [ஆமோஸ் 3](/bible/TOVBSI/AMO/3.html)
- [ஆமோஸ் 4](/bible/TOVBSI/AMO/4.html)
- [ஆமோஸ் 5](/bible/TOVBSI/AMO/5.html)
- [ஆமோஸ் 6](/bible/TOVBSI/AMO/6.html)
- [ஆமோஸ் 7](/bible/TOVBSI/AMO/7.html)
- [ஆமோஸ் 8](/bible/TOVBSI/AMO/8.html)
- [ஆமோஸ் 9](/bible/TOVBSI/AMO/9.html)
