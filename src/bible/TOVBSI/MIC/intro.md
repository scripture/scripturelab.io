## மீகா
- Number of chapters: 7
- Number of verses: 105
## Chapters
- [மீகா 1](/bible/TOVBSI/MIC/1.html)
- [மீகா 2](/bible/TOVBSI/MIC/2.html)
- [மீகா 3](/bible/TOVBSI/MIC/3.html)
- [மீகா 4](/bible/TOVBSI/MIC/4.html)
- [மீகா 5](/bible/TOVBSI/MIC/5.html)
- [மீகா 6](/bible/TOVBSI/MIC/6.html)
- [மீகா 7](/bible/TOVBSI/MIC/7.html)
