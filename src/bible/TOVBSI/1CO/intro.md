## 1 கொரிந்தியர்
- Number of chapters: 16
- Number of verses: 437
## Chapters
- [1 கொரிந்தியர் 1](/bible/TOVBSI/1CO/1.html)
- [1 கொரிந்தியர் 2](/bible/TOVBSI/1CO/2.html)
- [1 கொரிந்தியர் 3](/bible/TOVBSI/1CO/3.html)
- [1 கொரிந்தியர் 4](/bible/TOVBSI/1CO/4.html)
- [1 கொரிந்தியர் 5](/bible/TOVBSI/1CO/5.html)
- [1 கொரிந்தியர் 6](/bible/TOVBSI/1CO/6.html)
- [1 கொரிந்தியர் 7](/bible/TOVBSI/1CO/7.html)
- [1 கொரிந்தியர் 8](/bible/TOVBSI/1CO/8.html)
- [1 கொரிந்தியர் 9](/bible/TOVBSI/1CO/9.html)
- [1 கொரிந்தியர் 10](/bible/TOVBSI/1CO/10.html)
- [1 கொரிந்தியர் 11](/bible/TOVBSI/1CO/11.html)
- [1 கொரிந்தியர் 12](/bible/TOVBSI/1CO/12.html)
- [1 கொரிந்தியர் 13](/bible/TOVBSI/1CO/13.html)
- [1 கொரிந்தியர் 14](/bible/TOVBSI/1CO/14.html)
- [1 கொரிந்தியர் 15](/bible/TOVBSI/1CO/15.html)
- [1 கொரிந்தியர் 16](/bible/TOVBSI/1CO/16.html)
