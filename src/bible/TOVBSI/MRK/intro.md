## மாற்கு
- Number of chapters: 16
- Number of verses: 678
## Chapters
- [மாற்கு 1](/bible/TOVBSI/MRK/1.html)
- [மாற்கு 2](/bible/TOVBSI/MRK/2.html)
- [மாற்கு 3](/bible/TOVBSI/MRK/3.html)
- [மாற்கு 4](/bible/TOVBSI/MRK/4.html)
- [மாற்கு 5](/bible/TOVBSI/MRK/5.html)
- [மாற்கு 6](/bible/TOVBSI/MRK/6.html)
- [மாற்கு 7](/bible/TOVBSI/MRK/7.html)
- [மாற்கு 8](/bible/TOVBSI/MRK/8.html)
- [மாற்கு 9](/bible/TOVBSI/MRK/9.html)
- [மாற்கு 10](/bible/TOVBSI/MRK/10.html)
- [மாற்கு 11](/bible/TOVBSI/MRK/11.html)
- [மாற்கு 12](/bible/TOVBSI/MRK/12.html)
- [மாற்கு 13](/bible/TOVBSI/MRK/13.html)
- [மாற்கு 14](/bible/TOVBSI/MRK/14.html)
- [மாற்கு 15](/bible/TOVBSI/MRK/15.html)
- [மாற்கு 16](/bible/TOVBSI/MRK/16.html)
