## 1 தீமோத்தேயு
- Number of chapters: 6
- Number of verses: 113
## Chapters
- [1 தீமோத்தேயு 1](/bible/TOVBSI/1TI/1.html)
- [1 தீமோத்தேயு 2](/bible/TOVBSI/1TI/2.html)
- [1 தீமோத்தேயு 3](/bible/TOVBSI/1TI/3.html)
- [1 தீமோத்தேயு 4](/bible/TOVBSI/1TI/4.html)
- [1 தீமோத்தேயு 5](/bible/TOVBSI/1TI/5.html)
- [1 தீமோத்தேயு 6](/bible/TOVBSI/1TI/6.html)
