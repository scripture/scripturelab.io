## 1 தெசலோனிக்கேயர்
- Number of chapters: 5
- Number of verses: 89
## Chapters
- [1 தெசலோனிக்கேயர் 1](/bible/TOVBSI/1TH/1.html)
- [1 தெசலோனிக்கேயர் 2](/bible/TOVBSI/1TH/2.html)
- [1 தெசலோனிக்கேயர் 3](/bible/TOVBSI/1TH/3.html)
- [1 தெசலோனிக்கேயர் 4](/bible/TOVBSI/1TH/4.html)
- [1 தெசலோனிக்கேயர் 5](/bible/TOVBSI/1TH/5.html)
