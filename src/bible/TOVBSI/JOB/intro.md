## யோபு
- Number of chapters: 42
- Number of verses: 1070
## Chapters
- [யோபு 1](/bible/TOVBSI/JOB/1.html)
- [யோபு 2](/bible/TOVBSI/JOB/2.html)
- [யோபு 3](/bible/TOVBSI/JOB/3.html)
- [யோபு 4](/bible/TOVBSI/JOB/4.html)
- [யோபு 5](/bible/TOVBSI/JOB/5.html)
- [யோபு 6](/bible/TOVBSI/JOB/6.html)
- [யோபு 7](/bible/TOVBSI/JOB/7.html)
- [யோபு 8](/bible/TOVBSI/JOB/8.html)
- [யோபு 9](/bible/TOVBSI/JOB/9.html)
- [யோபு 10](/bible/TOVBSI/JOB/10.html)
- [யோபு 11](/bible/TOVBSI/JOB/11.html)
- [யோபு 12](/bible/TOVBSI/JOB/12.html)
- [யோபு 13](/bible/TOVBSI/JOB/13.html)
- [யோபு 14](/bible/TOVBSI/JOB/14.html)
- [யோபு 15](/bible/TOVBSI/JOB/15.html)
- [யோபு 16](/bible/TOVBSI/JOB/16.html)
- [யோபு 17](/bible/TOVBSI/JOB/17.html)
- [யோபு 18](/bible/TOVBSI/JOB/18.html)
- [யோபு 19](/bible/TOVBSI/JOB/19.html)
- [யோபு 20](/bible/TOVBSI/JOB/20.html)
- [யோபு 21](/bible/TOVBSI/JOB/21.html)
- [யோபு 22](/bible/TOVBSI/JOB/22.html)
- [யோபு 23](/bible/TOVBSI/JOB/23.html)
- [யோபு 24](/bible/TOVBSI/JOB/24.html)
- [யோபு 25](/bible/TOVBSI/JOB/25.html)
- [யோபு 26](/bible/TOVBSI/JOB/26.html)
- [யோபு 27](/bible/TOVBSI/JOB/27.html)
- [யோபு 28](/bible/TOVBSI/JOB/28.html)
- [யோபு 29](/bible/TOVBSI/JOB/29.html)
- [யோபு 30](/bible/TOVBSI/JOB/30.html)
- [யோபு 31](/bible/TOVBSI/JOB/31.html)
- [யோபு 32](/bible/TOVBSI/JOB/32.html)
- [யோபு 33](/bible/TOVBSI/JOB/33.html)
- [யோபு 34](/bible/TOVBSI/JOB/34.html)
- [யோபு 35](/bible/TOVBSI/JOB/35.html)
- [யோபு 36](/bible/TOVBSI/JOB/36.html)
- [யோபு 37](/bible/TOVBSI/JOB/37.html)
- [யோபு 38](/bible/TOVBSI/JOB/38.html)
- [யோபு 39](/bible/TOVBSI/JOB/39.html)
- [யோபு 40](/bible/TOVBSI/JOB/40.html)
- [யோபு 41](/bible/TOVBSI/JOB/41.html)
- [யோபு 42](/bible/TOVBSI/JOB/42.html)
