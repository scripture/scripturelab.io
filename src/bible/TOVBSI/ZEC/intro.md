## சகரியா
- Number of chapters: 14
- Number of verses: 211
## Chapters
- [சகரியா 1](/bible/TOVBSI/ZEC/1.html)
- [சகரியா 2](/bible/TOVBSI/ZEC/2.html)
- [சகரியா 3](/bible/TOVBSI/ZEC/3.html)
- [சகரியா 4](/bible/TOVBSI/ZEC/4.html)
- [சகரியா 5](/bible/TOVBSI/ZEC/5.html)
- [சகரியா 6](/bible/TOVBSI/ZEC/6.html)
- [சகரியா 7](/bible/TOVBSI/ZEC/7.html)
- [சகரியா 8](/bible/TOVBSI/ZEC/8.html)
- [சகரியா 9](/bible/TOVBSI/ZEC/9.html)
- [சகரியா 10](/bible/TOVBSI/ZEC/10.html)
- [சகரியா 11](/bible/TOVBSI/ZEC/11.html)
- [சகரியா 12](/bible/TOVBSI/ZEC/12.html)
- [சகரியா 13](/bible/TOVBSI/ZEC/13.html)
- [சகரியா 14](/bible/TOVBSI/ZEC/14.html)
