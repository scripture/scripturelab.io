## கலாத்தியர்
- Number of chapters: 6
- Number of verses: 149
## Chapters
- [கலாத்தியர் 1](/bible/TOVBSI/GAL/1.html)
- [கலாத்தியர் 2](/bible/TOVBSI/GAL/2.html)
- [கலாத்தியர் 3](/bible/TOVBSI/GAL/3.html)
- [கலாத்தியர் 4](/bible/TOVBSI/GAL/4.html)
- [கலாத்தியர் 5](/bible/TOVBSI/GAL/5.html)
- [கலாத்தியர் 6](/bible/TOVBSI/GAL/6.html)
