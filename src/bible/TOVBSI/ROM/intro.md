## ரோமர்
- Number of chapters: 16
- Number of verses: 433
## Chapters
- [ரோமர் 1](/bible/TOVBSI/ROM/1.html)
- [ரோமர் 2](/bible/TOVBSI/ROM/2.html)
- [ரோமர் 3](/bible/TOVBSI/ROM/3.html)
- [ரோமர் 4](/bible/TOVBSI/ROM/4.html)
- [ரோமர் 5](/bible/TOVBSI/ROM/5.html)
- [ரோமர் 6](/bible/TOVBSI/ROM/6.html)
- [ரோமர் 7](/bible/TOVBSI/ROM/7.html)
- [ரோமர் 8](/bible/TOVBSI/ROM/8.html)
- [ரோமர் 9](/bible/TOVBSI/ROM/9.html)
- [ரோமர் 10](/bible/TOVBSI/ROM/10.html)
- [ரோமர் 11](/bible/TOVBSI/ROM/11.html)
- [ரோமர் 12](/bible/TOVBSI/ROM/12.html)
- [ரோமர் 13](/bible/TOVBSI/ROM/13.html)
- [ரோமர் 14](/bible/TOVBSI/ROM/14.html)
- [ரோமர் 15](/bible/TOVBSI/ROM/15.html)
- [ரோமர் 16](/bible/TOVBSI/ROM/16.html)
