## 1 பேதுரு
- Number of chapters: 5
- Number of verses: 105
## Chapters
- [1 பேதுரு 1](/bible/TOVBSI/1PE/1.html)
- [1 பேதுரு 2](/bible/TOVBSI/1PE/2.html)
- [1 பேதுரு 3](/bible/TOVBSI/1PE/3.html)
- [1 பேதுரு 4](/bible/TOVBSI/1PE/4.html)
- [1 பேதுரு 5](/bible/TOVBSI/1PE/5.html)
