## பிரசங்கி
- Number of chapters: 12
- Number of verses: 222
## Chapters
- [பிரசங்கி 1](/bible/TOVBSI/ECC/1.html)
- [பிரசங்கி 2](/bible/TOVBSI/ECC/2.html)
- [பிரசங்கி 3](/bible/TOVBSI/ECC/3.html)
- [பிரசங்கி 4](/bible/TOVBSI/ECC/4.html)
- [பிரசங்கி 5](/bible/TOVBSI/ECC/5.html)
- [பிரசங்கி 6](/bible/TOVBSI/ECC/6.html)
- [பிரசங்கி 7](/bible/TOVBSI/ECC/7.html)
- [பிரசங்கி 8](/bible/TOVBSI/ECC/8.html)
- [பிரசங்கி 9](/bible/TOVBSI/ECC/9.html)
- [பிரசங்கி 10](/bible/TOVBSI/ECC/10.html)
- [பிரசங்கி 11](/bible/TOVBSI/ECC/11.html)
- [பிரசங்கி 12](/bible/TOVBSI/ECC/12.html)
