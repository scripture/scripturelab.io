## எபேசியர்
- Number of chapters: 6
- Number of verses: 155
## Chapters
- [எபேசியர் 1](/bible/TOVBSI/EPH/1.html)
- [எபேசியர் 2](/bible/TOVBSI/EPH/2.html)
- [எபேசியர் 3](/bible/TOVBSI/EPH/3.html)
- [எபேசியர் 4](/bible/TOVBSI/EPH/4.html)
- [எபேசியர் 5](/bible/TOVBSI/EPH/5.html)
- [எபேசியர் 6](/bible/TOVBSI/EPH/6.html)
