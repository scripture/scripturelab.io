## Micah
- Number of chapters: 7
- Number of verses: 105
## Chapters
- [Micah 1](/bible/ASV/MIC/1.html)
- [Micah 2](/bible/ASV/MIC/2.html)
- [Micah 3](/bible/ASV/MIC/3.html)
- [Micah 4](/bible/ASV/MIC/4.html)
- [Micah 5](/bible/ASV/MIC/5.html)
- [Micah 6](/bible/ASV/MIC/6.html)
- [Micah 7](/bible/ASV/MIC/7.html)
