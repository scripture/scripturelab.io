## Ephesians
- Number of chapters: 6
- Number of verses: 155
## Chapters
- [Ephesians 1](/bible/ASV/EPH/1.html)
- [Ephesians 2](/bible/ASV/EPH/2.html)
- [Ephesians 3](/bible/ASV/EPH/3.html)
- [Ephesians 4](/bible/ASV/EPH/4.html)
- [Ephesians 5](/bible/ASV/EPH/5.html)
- [Ephesians 6](/bible/ASV/EPH/6.html)
