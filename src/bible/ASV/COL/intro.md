## Colossians
- Number of chapters: 4
- Number of verses: 95
## Chapters
- [Colossians 1](/bible/ASV/COL/1.html)
- [Colossians 2](/bible/ASV/COL/2.html)
- [Colossians 3](/bible/ASV/COL/3.html)
- [Colossians 4](/bible/ASV/COL/4.html)
