## Malachi
- Number of chapters: 4
- Number of verses: 55
## Chapters
- [Malachi 1](/bible/ASV/MAL/1.html)
- [Malachi 2](/bible/ASV/MAL/2.html)
- [Malachi 3](/bible/ASV/MAL/3.html)
- [Malachi 4](/bible/ASV/MAL/4.html)
