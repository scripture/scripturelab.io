## Galatians
- Number of chapters: 6
- Number of verses: 149
## Chapters
- [Galatians 1](/bible/ASV/GAL/1.html)
- [Galatians 2](/bible/ASV/GAL/2.html)
- [Galatians 3](/bible/ASV/GAL/3.html)
- [Galatians 4](/bible/ASV/GAL/4.html)
- [Galatians 5](/bible/ASV/GAL/5.html)
- [Galatians 6](/bible/ASV/GAL/6.html)
