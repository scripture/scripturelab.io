## James
- Number of chapters: 5
- Number of verses: 108
## Chapters
- [James 1](/bible/ASV/JAS/1.html)
- [James 2](/bible/ASV/JAS/2.html)
- [James 3](/bible/ASV/JAS/3.html)
- [James 4](/bible/ASV/JAS/4.html)
- [James 5](/bible/ASV/JAS/5.html)
