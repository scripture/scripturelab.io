## Hosea
- Number of chapters: 14
- Number of verses: 197
## Chapters
- [Hosea 1](/bible/ASV/HOS/1.html)
- [Hosea 2](/bible/ASV/HOS/2.html)
- [Hosea 3](/bible/ASV/HOS/3.html)
- [Hosea 4](/bible/ASV/HOS/4.html)
- [Hosea 5](/bible/ASV/HOS/5.html)
- [Hosea 6](/bible/ASV/HOS/6.html)
- [Hosea 7](/bible/ASV/HOS/7.html)
- [Hosea 8](/bible/ASV/HOS/8.html)
- [Hosea 9](/bible/ASV/HOS/9.html)
- [Hosea 10](/bible/ASV/HOS/10.html)
- [Hosea 11](/bible/ASV/HOS/11.html)
- [Hosea 12](/bible/ASV/HOS/12.html)
- [Hosea 13](/bible/ASV/HOS/13.html)
- [Hosea 14](/bible/ASV/HOS/14.html)
