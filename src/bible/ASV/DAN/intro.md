## Daniel
- Number of chapters: 12
- Number of verses: 357
## Chapters
- [Daniel 1](/bible/ASV/DAN/1.html)
- [Daniel 2](/bible/ASV/DAN/2.html)
- [Daniel 3](/bible/ASV/DAN/3.html)
- [Daniel 4](/bible/ASV/DAN/4.html)
- [Daniel 5](/bible/ASV/DAN/5.html)
- [Daniel 6](/bible/ASV/DAN/6.html)
- [Daniel 7](/bible/ASV/DAN/7.html)
- [Daniel 8](/bible/ASV/DAN/8.html)
- [Daniel 9](/bible/ASV/DAN/9.html)
- [Daniel 10](/bible/ASV/DAN/10.html)
- [Daniel 11](/bible/ASV/DAN/11.html)
- [Daniel 12](/bible/ASV/DAN/12.html)
