## Ezra
- Number of chapters: 10
- Number of verses: 280
## Chapters
- [Ezra 1](/bible/ASV/EZR/1.html)
- [Ezra 2](/bible/ASV/EZR/2.html)
- [Ezra 3](/bible/ASV/EZR/3.html)
- [Ezra 4](/bible/ASV/EZR/4.html)
- [Ezra 5](/bible/ASV/EZR/5.html)
- [Ezra 6](/bible/ASV/EZR/6.html)
- [Ezra 7](/bible/ASV/EZR/7.html)
- [Ezra 8](/bible/ASV/EZR/8.html)
- [Ezra 9](/bible/ASV/EZR/9.html)
- [Ezra 10](/bible/ASV/EZR/10.html)
