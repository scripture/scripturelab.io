## Daniel
- Number of chapters: 12
- Number of verses: 357
## Chapters
- [Daniel 1](/bible/KJV/DAN/1.html)
- [Daniel 2](/bible/KJV/DAN/2.html)
- [Daniel 3](/bible/KJV/DAN/3.html)
- [Daniel 4](/bible/KJV/DAN/4.html)
- [Daniel 5](/bible/KJV/DAN/5.html)
- [Daniel 6](/bible/KJV/DAN/6.html)
- [Daniel 7](/bible/KJV/DAN/7.html)
- [Daniel 8](/bible/KJV/DAN/8.html)
- [Daniel 9](/bible/KJV/DAN/9.html)
- [Daniel 10](/bible/KJV/DAN/10.html)
- [Daniel 11](/bible/KJV/DAN/11.html)
- [Daniel 12](/bible/KJV/DAN/12.html)
