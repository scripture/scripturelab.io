## 2 Peter
- Number of chapters: 3
- Number of verses: 61
## Chapters
- [2 Peter 1](/bible/KJV/2PE/1.html)
- [2 Peter 2](/bible/KJV/2PE/2.html)
- [2 Peter 3](/bible/KJV/2PE/3.html)
