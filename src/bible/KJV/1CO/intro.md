## 1 Corinthians
- Number of chapters: 16
- Number of verses: 437
## Chapters
- [1 Corinthians 1](/bible/KJV/1CO/1.html)
- [1 Corinthians 2](/bible/KJV/1CO/2.html)
- [1 Corinthians 3](/bible/KJV/1CO/3.html)
- [1 Corinthians 4](/bible/KJV/1CO/4.html)
- [1 Corinthians 5](/bible/KJV/1CO/5.html)
- [1 Corinthians 6](/bible/KJV/1CO/6.html)
- [1 Corinthians 7](/bible/KJV/1CO/7.html)
- [1 Corinthians 8](/bible/KJV/1CO/8.html)
- [1 Corinthians 9](/bible/KJV/1CO/9.html)
- [1 Corinthians 10](/bible/KJV/1CO/10.html)
- [1 Corinthians 11](/bible/KJV/1CO/11.html)
- [1 Corinthians 12](/bible/KJV/1CO/12.html)
- [1 Corinthians 13](/bible/KJV/1CO/13.html)
- [1 Corinthians 14](/bible/KJV/1CO/14.html)
- [1 Corinthians 15](/bible/KJV/1CO/15.html)
- [1 Corinthians 16](/bible/KJV/1CO/16.html)
