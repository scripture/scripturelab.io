## 1 John
- Number of chapters: 5
- Number of verses: 105
## Chapters
- [1 John 1](/bible/KJV/1JN/1.html)
- [1 John 2](/bible/KJV/1JN/2.html)
- [1 John 3](/bible/KJV/1JN/3.html)
- [1 John 4](/bible/KJV/1JN/4.html)
- [1 John 5](/bible/KJV/1JN/5.html)
