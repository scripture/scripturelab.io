## Hosea
- Number of chapters: 14
- Number of verses: 197
## Chapters
- [Hosea 1](/bible/KJV/HOS/1.html)
- [Hosea 2](/bible/KJV/HOS/2.html)
- [Hosea 3](/bible/KJV/HOS/3.html)
- [Hosea 4](/bible/KJV/HOS/4.html)
- [Hosea 5](/bible/KJV/HOS/5.html)
- [Hosea 6](/bible/KJV/HOS/6.html)
- [Hosea 7](/bible/KJV/HOS/7.html)
- [Hosea 8](/bible/KJV/HOS/8.html)
- [Hosea 9](/bible/KJV/HOS/9.html)
- [Hosea 10](/bible/KJV/HOS/10.html)
- [Hosea 11](/bible/KJV/HOS/11.html)
- [Hosea 12](/bible/KJV/HOS/12.html)
- [Hosea 13](/bible/KJV/HOS/13.html)
- [Hosea 14](/bible/KJV/HOS/14.html)
