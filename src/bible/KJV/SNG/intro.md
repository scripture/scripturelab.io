## Song of Solomon
- Number of chapters: 8
- Number of verses: 117
## Chapters
- [Song of Solomon 1](/bible/KJV/SNG/1.html)
- [Song of Solomon 2](/bible/KJV/SNG/2.html)
- [Song of Solomon 3](/bible/KJV/SNG/3.html)
- [Song of Solomon 4](/bible/KJV/SNG/4.html)
- [Song of Solomon 5](/bible/KJV/SNG/5.html)
- [Song of Solomon 6](/bible/KJV/SNG/6.html)
- [Song of Solomon 7](/bible/KJV/SNG/7.html)
- [Song of Solomon 8](/bible/KJV/SNG/8.html)
