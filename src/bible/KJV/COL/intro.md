## Colossians
- Number of chapters: 4
- Number of verses: 95
## Chapters
- [Colossians 1](/bible/KJV/COL/1.html)
- [Colossians 2](/bible/KJV/COL/2.html)
- [Colossians 3](/bible/KJV/COL/3.html)
- [Colossians 4](/bible/KJV/COL/4.html)
