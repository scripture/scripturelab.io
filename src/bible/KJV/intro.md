# KJV
- Full name: King James Version
- Language: English
- Year: 1611
- License: Public Domain
- Description:
	- https://en.wikipedia.org/wiki/King_James_Version
## Books
### Old Testament
- [Genesis](GEN/intro.html)
- [Exodus](EXO/intro.html)
- [Leviticus](LEV/intro.html)
- [Numbers](NUM/intro.html)
- [Deuteronomy](DEU/intro.html)
- [Joshua](JOS/intro.html)
- [Judges](JDG/intro.html)
- [Ruth](RUT/intro.html)
- [1 Samuel](1SA/intro.html)
- [2 Samuel](2SA/intro.html)
- [1 Kings](1KI/intro.html)
- [2 Kings](2KI/intro.html)
- [1 Chronicles](1CH/intro.html)
- [2 Chronicles](2CH/intro.html)
- [Ezra](EZR/intro.html)
- [Nehemiah](NEH/intro.html)
- [Esther](EST/intro.html)
- [Job](JOB/intro.html)
- [Psalms](PSA/intro.html)
- [Proverbs](PRO/intro.html)
- [Ecclesiastes](ECC/intro.html)
- [Song of Solomon](SNG/intro.html)
- [Isaiah](ISA/intro.html)
- [Jeremiah](JER/intro.html)
- [Lamentations](LAM/intro.html)
- [Ezekiel](EZK/intro.html)
- [Daniel](DAN/intro.html)
- [Hosea](HOS/intro.html)
- [Joel](JOL/intro.html)
- [Amos](AMO/intro.html)
- [Obadiah](OBA/intro.html)
- [Jonah](JON/intro.html)
- [Micah](MIC/intro.html)
- [Nahum](NAM/intro.html)
- [Habakkuk](HAB/intro.html)
- [Zephaniah](ZEP/intro.html)
- [Haggai](HAG/intro.html)
- [Zechariah](ZEC/intro.html)
- [Malachi](MAL/intro.html)
### New Testament
- [Matthew](MAT/intro.html)
- [Mark](MRK/intro.html)
- [Luke](LUK/intro.html)
- [John](JHN/intro.html)
- [Acts](ACT/intro.html)
- [Romans](ROM/intro.html)
- [1 Corinthians](1CO/intro.html)
- [2 Corinthians](2CO/intro.html)
- [Galatians](GAL/intro.html)
- [Ephesians](EPH/intro.html)
- [Philippians](PHP/intro.html)
- [Colossians](COL/intro.html)
- [1 Thessalonians](1TH/intro.html)
- [2 Thessalonians](2TH/intro.html)
- [1 Timothy](1TI/intro.html)
- [2 Timothy](2TI/intro.html)
- [Titus](TIT/intro.html)
- [Philemon](PHM/intro.html)
- [Hebrews](HEB/intro.html)
- [James](JAS/intro.html)
- [1 Peter](1PE/intro.html)
- [2 Peter](2PE/intro.html)
- [1 John](1JN/intro.html)
- [2 John](2JN/intro.html)
- [3 John](3JN/intro.html)
- [Jude](JUD/intro.html)
- [Revelations](REV/intro.html)
