## Deuteronomy
- Number of chapters: 34
- Number of verses: 959
## Chapters
- [Deuteronomy 1](/bible/KJV/DEU/1.html)
- [Deuteronomy 2](/bible/KJV/DEU/2.html)
- [Deuteronomy 3](/bible/KJV/DEU/3.html)
- [Deuteronomy 4](/bible/KJV/DEU/4.html)
- [Deuteronomy 5](/bible/KJV/DEU/5.html)
- [Deuteronomy 6](/bible/KJV/DEU/6.html)
- [Deuteronomy 7](/bible/KJV/DEU/7.html)
- [Deuteronomy 8](/bible/KJV/DEU/8.html)
- [Deuteronomy 9](/bible/KJV/DEU/9.html)
- [Deuteronomy 10](/bible/KJV/DEU/10.html)
- [Deuteronomy 11](/bible/KJV/DEU/11.html)
- [Deuteronomy 12](/bible/KJV/DEU/12.html)
- [Deuteronomy 13](/bible/KJV/DEU/13.html)
- [Deuteronomy 14](/bible/KJV/DEU/14.html)
- [Deuteronomy 15](/bible/KJV/DEU/15.html)
- [Deuteronomy 16](/bible/KJV/DEU/16.html)
- [Deuteronomy 17](/bible/KJV/DEU/17.html)
- [Deuteronomy 18](/bible/KJV/DEU/18.html)
- [Deuteronomy 19](/bible/KJV/DEU/19.html)
- [Deuteronomy 20](/bible/KJV/DEU/20.html)
- [Deuteronomy 21](/bible/KJV/DEU/21.html)
- [Deuteronomy 22](/bible/KJV/DEU/22.html)
- [Deuteronomy 23](/bible/KJV/DEU/23.html)
- [Deuteronomy 24](/bible/KJV/DEU/24.html)
- [Deuteronomy 25](/bible/KJV/DEU/25.html)
- [Deuteronomy 26](/bible/KJV/DEU/26.html)
- [Deuteronomy 27](/bible/KJV/DEU/27.html)
- [Deuteronomy 28](/bible/KJV/DEU/28.html)
- [Deuteronomy 29](/bible/KJV/DEU/29.html)
- [Deuteronomy 30](/bible/KJV/DEU/30.html)
- [Deuteronomy 31](/bible/KJV/DEU/31.html)
- [Deuteronomy 32](/bible/KJV/DEU/32.html)
- [Deuteronomy 33](/bible/KJV/DEU/33.html)
- [Deuteronomy 34](/bible/KJV/DEU/34.html)
