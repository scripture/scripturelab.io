## 1 Peter
- Number of chapters: 5
- Number of verses: 105
## Chapters
- [1 Peter 1](/bible/KJV/1PE/1.html)
- [1 Peter 2](/bible/KJV/1PE/2.html)
- [1 Peter 3](/bible/KJV/1PE/3.html)
- [1 Peter 4](/bible/KJV/1PE/4.html)
- [1 Peter 5](/bible/KJV/1PE/5.html)
