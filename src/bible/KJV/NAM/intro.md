## Nahum
- Number of chapters: 3
- Number of verses: 47
## Chapters
- [Nahum 1](/bible/KJV/NAM/1.html)
- [Nahum 2](/bible/KJV/NAM/2.html)
- [Nahum 3](/bible/KJV/NAM/3.html)
