## 2 Thessalonians
- Number of chapters: 3
- Number of verses: 47
## Chapters
- [2 Thessalonians 1](/bible/KJV/2TH/1.html)
- [2 Thessalonians 2](/bible/KJV/2TH/2.html)
- [2 Thessalonians 3](/bible/KJV/2TH/3.html)
