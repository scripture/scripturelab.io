## Ezra
- Number of chapters: 10
- Number of verses: 280
## Chapters
- [Ezra 1](/bible/KJV/EZR/1.html)
- [Ezra 2](/bible/KJV/EZR/2.html)
- [Ezra 3](/bible/KJV/EZR/3.html)
- [Ezra 4](/bible/KJV/EZR/4.html)
- [Ezra 5](/bible/KJV/EZR/5.html)
- [Ezra 6](/bible/KJV/EZR/6.html)
- [Ezra 7](/bible/KJV/EZR/7.html)
- [Ezra 8](/bible/KJV/EZR/8.html)
- [Ezra 9](/bible/KJV/EZR/9.html)
- [Ezra 10](/bible/KJV/EZR/10.html)
