## Ruth
- Number of chapters: 4
- Number of verses: 85
## Chapters
- [Ruth 1](/bible/KJV/RUT/1.html)
- [Ruth 2](/bible/KJV/RUT/2.html)
- [Ruth 3](/bible/KJV/RUT/3.html)
- [Ruth 4](/bible/KJV/RUT/4.html)
