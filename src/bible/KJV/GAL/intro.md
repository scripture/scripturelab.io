## Galatians
- Number of chapters: 6
- Number of verses: 149
## Chapters
- [Galatians 1](/bible/KJV/GAL/1.html)
- [Galatians 2](/bible/KJV/GAL/2.html)
- [Galatians 3](/bible/KJV/GAL/3.html)
- [Galatians 4](/bible/KJV/GAL/4.html)
- [Galatians 5](/bible/KJV/GAL/5.html)
- [Galatians 6](/bible/KJV/GAL/6.html)
