## Romans
- Number of chapters: 16
- Number of verses: 433
## Chapters
- [Romans 1](/bible/KJV/ROM/1.html)
- [Romans 2](/bible/KJV/ROM/2.html)
- [Romans 3](/bible/KJV/ROM/3.html)
- [Romans 4](/bible/KJV/ROM/4.html)
- [Romans 5](/bible/KJV/ROM/5.html)
- [Romans 6](/bible/KJV/ROM/6.html)
- [Romans 7](/bible/KJV/ROM/7.html)
- [Romans 8](/bible/KJV/ROM/8.html)
- [Romans 9](/bible/KJV/ROM/9.html)
- [Romans 10](/bible/KJV/ROM/10.html)
- [Romans 11](/bible/KJV/ROM/11.html)
- [Romans 12](/bible/KJV/ROM/12.html)
- [Romans 13](/bible/KJV/ROM/13.html)
- [Romans 14](/bible/KJV/ROM/14.html)
- [Romans 15](/bible/KJV/ROM/15.html)
- [Romans 16](/bible/KJV/ROM/16.html)
