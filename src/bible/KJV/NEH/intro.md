## Nehemiah
- Number of chapters: 13
- Number of verses: 406
## Chapters
- [Nehemiah 1](/bible/KJV/NEH/1.html)
- [Nehemiah 2](/bible/KJV/NEH/2.html)
- [Nehemiah 3](/bible/KJV/NEH/3.html)
- [Nehemiah 4](/bible/KJV/NEH/4.html)
- [Nehemiah 5](/bible/KJV/NEH/5.html)
- [Nehemiah 6](/bible/KJV/NEH/6.html)
- [Nehemiah 7](/bible/KJV/NEH/7.html)
- [Nehemiah 8](/bible/KJV/NEH/8.html)
- [Nehemiah 9](/bible/KJV/NEH/9.html)
- [Nehemiah 10](/bible/KJV/NEH/10.html)
- [Nehemiah 11](/bible/KJV/NEH/11.html)
- [Nehemiah 12](/bible/KJV/NEH/12.html)
- [Nehemiah 13](/bible/KJV/NEH/13.html)
