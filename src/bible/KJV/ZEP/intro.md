## Zephaniah
- Number of chapters: 3
- Number of verses: 53
## Chapters
- [Zephaniah 1](/bible/KJV/ZEP/1.html)
- [Zephaniah 2](/bible/KJV/ZEP/2.html)
- [Zephaniah 3](/bible/KJV/ZEP/3.html)
