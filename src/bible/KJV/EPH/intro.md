## Ephesians
- Number of chapters: 6
- Number of verses: 155
## Chapters
- [Ephesians 1](/bible/KJV/EPH/1.html)
- [Ephesians 2](/bible/KJV/EPH/2.html)
- [Ephesians 3](/bible/KJV/EPH/3.html)
- [Ephesians 4](/bible/KJV/EPH/4.html)
- [Ephesians 5](/bible/KJV/EPH/5.html)
- [Ephesians 6](/bible/KJV/EPH/6.html)
