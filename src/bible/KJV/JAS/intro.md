## James
- Number of chapters: 5
- Number of verses: 108
## Chapters
- [James 1](/bible/KJV/JAS/1.html)
- [James 2](/bible/KJV/JAS/2.html)
- [James 3](/bible/KJV/JAS/3.html)
- [James 4](/bible/KJV/JAS/4.html)
- [James 5](/bible/KJV/JAS/5.html)
