## Micah
- Number of chapters: 7
- Number of verses: 105
## Chapters
- [Micah 1](/bible/KJV/MIC/1.html)
- [Micah 2](/bible/KJV/MIC/2.html)
- [Micah 3](/bible/KJV/MIC/3.html)
- [Micah 4](/bible/KJV/MIC/4.html)
- [Micah 5](/bible/KJV/MIC/5.html)
- [Micah 6](/bible/KJV/MIC/6.html)
- [Micah 7](/bible/KJV/MIC/7.html)
