## Malachi
- Number of chapters: 4
- Number of verses: 55
## Chapters
- [Malachi 1](/bible/KJV/MAL/1.html)
- [Malachi 2](/bible/KJV/MAL/2.html)
- [Malachi 3](/bible/KJV/MAL/3.html)
- [Malachi 4](/bible/KJV/MAL/4.html)
