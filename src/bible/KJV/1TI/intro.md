## 1 Timothy
- Number of chapters: 6
- Number of verses: 113
## Chapters
- [1 Timothy 1](/bible/KJV/1TI/1.html)
- [1 Timothy 2](/bible/KJV/1TI/2.html)
- [1 Timothy 3](/bible/KJV/1TI/3.html)
- [1 Timothy 4](/bible/KJV/1TI/4.html)
- [1 Timothy 5](/bible/KJV/1TI/5.html)
- [1 Timothy 6](/bible/KJV/1TI/6.html)
