# Scripture

This repo contains code to generate mdbook for all bibles from [bible-api](https://github.com/berinaniesh/bible-api). 

Bible is generated using [mdbook](https://rust-lang.github.io/mdBook/)

## How To

```
mdbook serve -n 0.0.0.0
```